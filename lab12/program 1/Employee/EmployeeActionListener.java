
/*
authoe smith christian 
rollno 201501022
program actionlistener for employee
*/
package lab12.Employee;
import lab12.Sql.Employee;
import lab12.Sql.Sql;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class EmployeeActionListener extends Exception implements ActionListener
{
    public Employee_GUI obj1;
    public Sql obj2 = new Sql(); 
     
    public Employee emp =  new Employee();
    
    public EmployeeActionListener(Employee_GUI temp)
    {
        obj1 = temp;
        obj2.connection_emp();
    }
    int cnt=1;
    public void actionPerformed(ActionEvent e) 
    {
        try
        {
             
            if(obj1.btnfind.getText().equals(e.getActionCommand()))
            {
                for(int i=1;i<obj1.model.getRowCount();i++)
                {
                    if(obj1.txtfind.getText().equals(obj1.model.getValueAt(i,1).toString()))
                    {
                        obj1.txtId.setText(obj1.model.getValueAt(i,0).toString());
                        obj1.txtName.setText(obj1.model.getValueAt(i,1).toString());
                        obj1.txtAdd.setText(obj1.model.getValueAt(i,2).toString());
                        obj1.txtGender.setText(obj1.model.getValueAt(i,3).toString());
                        obj1.txtDob.setText(obj1.model.getValueAt(i,4).toString());
                        obj1.txtContact.setText(obj1.model.getValueAt(i,5).toString());
                        obj1.frame2.setVisible(true);
                    }
                }
            }
            else if(obj1.btnsalary.getText().equals(e.getActionCommand()))
            {
                obj1.frame3.setVisible(true);
            }
            else if(obj1.btncount.getText().equals(e.getActionCommand()))
            {
                    double sal = Double.parseDouble(obj1.txtsal.getText());
                    System.out.println(sal);
            
                     obj1.txtns.setText(Double.toString(emp.getns(sal)));
                     obj1.txtgs.setText(Double.toString(emp.getgs(sal)));
            }            
            else if(obj1.btninsert.getText().equals(e.getActionCommand()))
            {
                String name = obj1.txtname.getText();
                String add = obj1.txtadd.getText();
                String gender = (obj1.rmale.isSelected())?"M":"F";
                String dob = obj1.txtdob.getText();
                String contact= obj1.txtcontact.getText();
                obj2.insert(name,add,gender,dob,contact);
                obj1.txtid.setText("");
                obj1.txtname.setText("");
                obj1.txtadd.setText("");
                obj1.txtdob.setText("");
                obj1.txtcontact.setText("");
            }
            else if(obj1.btndisplay.getText().equals(e.getActionCommand()))
            {
                obj2.dis();
            }
            else if(obj1.btnupdate.getText().equals(e.getActionCommand()))
            {
                 obj1.frm1.setVisible(true);
            }
        }
        catch(ClassNotFoundException w)
        {
            System.out.println("Class not found ");
        }
        catch(SQLException w)
        {
            System.out.println("SQL exception   "+w.getMessage());
        }
        catch(Exception w)
          {
            System.out.println("ERROR"+w.getMessage());
        }
    }
    
}
