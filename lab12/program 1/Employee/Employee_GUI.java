
/*
authoe smith christian 
rollno 201501022
program GUi for employee
*/

package lab12.Employee;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.table.*;

public class Employee_GUI 
{
    String[]  header = {"Id","Name","Address","Gender","Date of birth","Contact No"};
    String[][] data={{"Id","Name","Address","Gender","Date of birth","Contact No"}};
    public DefaultTableModel model = new DefaultTableModel(data,header);
    public JTable table = new JTable(model);
    JLabel lbl,lblid,lblname,lbladd,lblgender,lbldob,lblcontact;
    public JTextField txtid,txtname,txtadd,txtdob,txtcontact,txtfind,txtsal,txtdelete,txtupdate,txtId1,txtName1,txtAdd1,txtGender1,txtDob1,txtContact1;
    ButtonGroup rgender,rgender1;
    JRadioButton rmale,rfemale,rother,rother1,rmale1,rfemale1;
    JButton btninsert,btnfind,btndisplay,btncount,btnsalary,btndelete,btnupdate,btnsubmit;
    JLabel txtId,txtName,txtAdd,txtGender,txtDob,txtContact,lblsal,lblgs,lblns,txtgs,txtns;
    JFrame frame2 = new JFrame();
    JFrame frame3 = new JFrame();
    JFrame frame4 = new JFrame();
    JFrame frm1 = new JFrame();
    JPanel panel1 = new JPanel();
    JPanel panel5 = new JPanel();
    JPanel p = new JPanel();    
    public Employee_GUI()
    {                   // intitial frame
        JFrame frame1 = new JFrame("Employee");
        frame1.setLayout(new BorderLayout());
	frame1.setSize(600, 400);
        frame1.setBackground(Color.DARK_GRAY);
	//initial panel
        JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(14,2));
	panel.setSize(600, 400);
	panel.setBackground(Color.DARK_GRAY);
        
        JPanel panel2 = new JPanel();
        //labels
   
       
        lblname = new JLabel("Name");
        lbladd = new JLabel("Address");
        lblgender = new JLabel("Gender");
        lbldob = new JLabel("DOB");
        lblcontact = new JLabel("Contact no");
        //textfields
      
        txtname = new JTextField(20);
        txtadd = new JTextField(25);
        //buttongroup for gender
        rgender = new ButtonGroup();
        rmale = new JRadioButton("Male");
        rfemale = new JRadioButton("Female");
        rother = new JRadioButton("other");
        rgender.add(rmale);
        rgender.add(rfemale);
        rgender.add(rother);
        txtdob = new JTextField(10);
        txtcontact = new JTextField(10);
        JLabel l = new JLabel();
        btninsert = new JButton("Insert");
        JLabel l1 = new JLabel();
        JLabel l2 = new JLabel();
        JLabel l5 = new JLabel();
        btnfind = new JButton("Find");
        txtfind = new JTextField(20);
        JLabel l3 = new JLabel();
        JLabel l4 = new JLabel();
        //button
        btndisplay = new JButton("Display"); 
        btnsalary = new JButton("Count Salary");
        btndelete = new JButton("Delete");
        txtdelete = new JTextField(10);
        btnupdate = new JButton("Update");
        txtupdate = new JTextField(10);
        //adding controls to panels
    
        panel.add(lblname);
        panel.add(txtname);
        panel.add(lbladd);
        panel.add(txtadd);
        panel.add(lblgender);
        panel.add(rmale);
        panel.add(rother);
        panel.add(rfemale);
        panel.add(lbldob);
        panel.add(txtdob);
        panel.add(lblcontact);
        panel.add(txtcontact);
        panel.add(l);
        panel.add(btninsert);
        panel.add(l1);
        panel.add(l2);
        panel.add(btnfind);
        panel.add(txtfind);
        panel.add(l3);
        panel.add(l4);
        panel.add(btndisplay);
        panel.add(btnsalary);
        panel.add(btndelete);
        panel.add(txtdelete);
        panel.add(btnupdate);
        panel.add(txtupdate);
        panel2.add(lbl);
       
        
        frame1.setVisible(true);
	frame1.add(panel);
        frame1.add(panel2,BorderLayout.NORTH);
        
        
        frame2.setLayout(new BorderLayout());
        frame2.setSize(600, 300);
	frame2.setLocationRelativeTo(null);
	
	panel1.setLayout(new GridLayout(10,2));
	panel1.setSize(600, 300);
	panel1.setBackground(Color.CYAN);

        JPanel panel3 = new JPanel();
        
        JLabel lbl1 = new JLabel("Details");
        JLabel lblId = new JLabel("Id");
        txtId = new JLabel(txtid.getText());
        JLabel lblName = new JLabel("Name");
        txtName = new JLabel(txtname.getText());
        JLabel lblAdd = new JLabel("Address");
        txtAdd = new JLabel(txtadd.getText());
        JLabel lblGender = new JLabel("Gender");
        txtGender = new JLabel((rmale.isSelected())?"Male":((rfemale.isSelected())?"Female":((rother.isSelected())?"other":null)));
        JLabel lblDob = new JLabel("DOB");
        txtDob = new JLabel(txtdob.getText());
        JLabel lblContact = new JLabel("Mob No");
        txtContact =new JLabel(txtcontact.getText());
        //adding controls to panel
        panel1.add(lblId);
        panel1.add(txtId);
        panel1.add(lblName);
        panel1.add(txtName);
        panel1.add(lblAdd);
        panel1.add(txtAdd);
        panel1.add(lblGender);
        panel1.add(txtGender);
        panel1.add(lblDob);
        panel1.add(txtDob);
        panel1.add(lblContact);
        panel1.add(txtContact);
        
        panel3.add(lbl1);
        
        frm1.setLayout(new FlowLayout());
        frm1.setSize(600, 300);
	frm1.setLocationRelativeTo(null);
	
        p.setLayout(new GridLayout(7,2));
        p.setBackground(Color.DARK_GRAY);
        
        lblname = new JLabel("Name");
        lbladd = new JLabel("Address");
        lblgender = new JLabel("Gender");
        lbldob = new JLabel("Date of birth");
        lblcontact = new JLabel("Contact no");
        txtName1 = new JTextField(9);
        txtAdd1 = new JTextField(9);
        rgender1 = new ButtonGroup();
        rmale1 = new JRadioButton("Male");
        rfemale1 = new JRadioButton("Female");
        rother1=new JRadioButton("other");
        rgender.add(rmale1);
        rgender.add(rfemale1);
        rgender.add(rother);
        txtDob1 = new JTextField(9);
        txtContact1 = new JTextField(9);
        JLabel a3 = new JLabel();
        btnsubmit = new JButton("Submit");
        
        p.add(lblname);
        p.add(txtName1);
        p.add(lbladd);
        p.add(txtAdd1);
        p.add(lblgender);
        p.add(rother1);
        p.add(rmale1);
        p.add(rfemale1);
        p.add(lbldob);
        p.add(txtDob1);
        p.add(lblContact);
        p.add(txtContact1);
        p.add(btnsubmit);
        
       frm1.add(p);
       frm1.setVisible(false);
      
        
        frame3.setLayout(new GridLayout(4,2));
        frame3.setSize(300, 300);
	frame3.setLocationRelativeTo(null);
        
        JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayout(4,2));
        panel4.setBackground(Color.CYAN);
        
        lblsal = new JLabel("Basic Salary");
        lblns = new JLabel("Net Salary");
        lblgs = new JLabel("Gross Salary");
        txtsal = new JTextField(9);
        txtgs = new JLabel("");
        txtns = new JLabel("");
        JLabel j = new JLabel();
        btncount = new JButton("Count");
        
        panel4.add(lblsal);
        panel4.add(txtsal);
        panel4.add(j);
        panel4.add(btncount);
        panel4.add(lblns);
        panel4.add(txtns);
        panel4.add(lblgs);
        panel4.add(txtgs);
        frame3.add(panel4);
        
        frame4.setLayout(new FlowLayout());
        frame4.setSize(500, 200);
	frame4.setLocationRelativeTo(null);
        

        panel5.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        table.setVisible(true);
        panel5.add(table);
        frame4.add(panel5);
        
        EmployeeActionListener listener = new EmployeeActionListener(this);
       //event handling for several button
        btndisplay.addActionListener(listener);
        btnsalary.addActionListener(listener);
        btncount.addActionListener(listener);
        btninsert.addActionListener(listener);
        btnfind.addActionListener(listener);
        btnupdate.addActionListener(listener);
        btndelete.addActionListener(listener);
    }
}
