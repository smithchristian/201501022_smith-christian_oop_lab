
/*
authoe smith christian 
rollno 201501022
program sql for employee
*/
package lab12.Sql;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lab12.Employee.Employee_GUI;


public class Sql extends Employee
{
    Employee_GUI a = new Employee_GUI();
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    String sql;    
    public void connection_emp()
    {

        try {
            System.out.println("conneting to Database...");
            Class.forName("com.mysql.jdbc.Driver");
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/database","root","smith1997");
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            System.out.println("Connection Successful");
            
            
            System.out.println("Creating statement...");
            try {
                statement = connection.createStatement();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void insert(String name,String add,String gender,String dob,String contact) 
    {
        try
        {
            sql = "INSERT INTO `database`.tblemployee VALUES(null,'" + name + "', '" + add + "', '" + gender + "','" + dob + "','"+ contact +"')";
            System.out.println(sql);
            statement.execute(sql);
            statement.close();
            connection.close();
        } 
        catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    public void update()
    {
        try
        {
          
            System.out.println(sql);
            statement.execute(sql);
            statement.close();
            connection.close();
        } 
        catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    public void delete()
    {
        
    }
    public void dis() throws ClassNotFoundException, SQLException
    {
        sql = "SELECT * from tblemployee";
      
        rs = statement.executeQuery(sql);
      //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
         String name = rs.getString("Name");
         int id = rs.getInt("Id");
         String address = rs.getString("Address");
         String gender = rs.getString("Gender");
         String dob = rs.getString("Dob");
         String contact = rs.getString("Contact_no");
         //Display values
         System.out.println("Id            : " + id);
         System.out.println("Name          : " + name);
         System.out.println("Address       : " + address);
         System.out.println("Gender        : " + gender);
         System.out.println("DOB           : " + dob);
         System.out.println("Contact No    : " + contact);         
      }
      rs.close();
    }
    
    
}