/* Author Smith Christian
date.26.1.2016  */
import java.util.*;
class customer
{
    private String name,add,mob_no;
    private int rup;
    public customer()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter ur name: ");
        name=in.next();
        System.out.println("Enter ur address: ");
        add=in.next();
        do
        {
            System.out.println("Enter contact number: ");
            mob_no=in.next();
        }while(mob_no.length()!=10);
        do
        {
            System.out.println("Enter starting ruppes:  ");
            rup=in.nextInt();
        }while(rup<1000);
    }
    public int get_rup()
    {
        return rup;
    }
    public void display()
    {
        System.out.println("Name            :  "+name);
        System.out.println("Address         :  "+add);
        System.out.println("Contact number  :  "+mob_no);
        System.out.println("Balance         :  "+rup);
    }
};
class account
{
    private int acc_num,acc,branch_id;
    private String acc_type,branch;
    public account()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter account number: ");
        acc_num=in.nextInt();
        System.out.println("1: Saving");
        System.out.println("2: Current");
        System.out.println("Choose ur account type ");
        acc=in.nextInt();
        if(acc==1)
            acc_type="Saving";
        else if(acc==2)
            acc_type="Current";
        else
            System.out.println("Invalid Choice");
        do
        {
            System.out.println("1 : ashram road\n2 : naranpura\n3 : satellite\n4 : memnagar");
            System.out.println("Choose ur branch:  ");
            branch_id=in.nextInt();
            switch(branch_id)
            {
                case 1:
                    branch="ashram road";
                    break;
                case 2:
                    branch="naranpura";
                    break;
                case 3:
                    branch="satellite";
                    break;
                case 4:
                    branch="memnagar";
                    break;
                default:
                    System.out.println("Wrong Cgoice!!!");
            }
        }while(branch_id!=1 && branch_id!=2 && branch_id!=3 && branch_id!=4);
    }
    public void display()
    {
        System.out.println("A/c number     "+acc_num);
        System.out.println("Branch         "+branch);
        System.out.println("A/c type       "+acc_type);
    }
};
class trans extends customer
{
    private int a,ch,amt;
    Scanner in= new Scanner(System.in);
    public trans()
    {
        System.out.println("1 : To withdraw\n2 : To deposit");
        System.out.println("Enter ur choice:  ");
        ch=in.nextInt();
        System.out.println("Your current balance   "+get_rup());
        switch(ch)
        {
            case 1:
                do
                {
                    System.out.println("Enter amount to withdraw from account:  ");
                    amt=in.nextInt();
                    a=get_rup();
                }while(amt>a);
                a=a-amt;
                break;
            case 2:
                System.out.println("Enter amount to deposit in account:  ");
                amt=in.nextInt();
                a=get_rup();
                a=a+amt;
                break;
            default:
                a=get_rup();
                System.out.println("Invalid Choice!!!");
        }
    }
    public void display()
    {
        super.display();
        System.out.println("Your current balance is "+a);
    }
};
class pro1
{
    public static void main(String args[])
    {
        int ch;
        do
        {
            Scanner in = new Scanner(System.in);
            System.out.println("\tMENU\t");
            System.out.println("1 : customer details");
            System.out.println("2 : account details");
            System.out.println("3 : money transection");
            System.out.println("0 : exit");
            System.out.println("Enter your choice: ");
            ch=in.nextInt();
            switch(ch)
            {
                case 1:
                    customer s = new customer();
                    s.display();
                    break;
                case 2:
                    account s = new account();
                    s.display();
                    break;
                case 3:
                    trans s = new trans();
                    System.out.println("\n");
                    s.display();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Invalid Choice!!");
            }
        }while(ch!=0);
    }
}
