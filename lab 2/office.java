/* Author Smith Christian
date.26.1.2016  */
import java.util.Scanner;
class office
{
	private static Scanner in;
	public static void main(String[] args)
	{
	    int choice;
		in = new Scanner(System.in);
		Number temp=new Number();
		LinkedList list=new LinkedList();
		while(true)
		{
			System.out.println("Enter Number: ");
			double d=in.nextDouble();
			temp=new Number(d);
			list.add(temp);
			System.out.println("Enter Another Number? ");
			int ch=in.nextInt();
			if(ch==0)
			{
				break;
			}
		}
		System.out.println("\n1 : calculate minimum\n2 : calculate maximum\n3 : calculate mean\n4 : calculate standard deviation\n5 : exit");
		System.out.println("Enter ur choice: ");
		choice=in.nextInt();
		switch(choice)
		{
            case 1:
                System.out.println("Min: "+list.min());
                break;
            case 2:
                System.out.println("Max: "+list.max());
                break;
            case 3:
                System.out.println("Mean: "+list.mean());
                break;
            case 4:
                System.out.println("Standard Deviation: " +list.sd());
                break;
            case 5:
                System.exit(0);
            default:
                System.out.println("Wrong choice!!");
		}
	}
}
class LinkedList
{
	private Number data = null;
	private LinkedList next = null;
	public void add(Number newData)
	{
		if(data == null)
			this.data = newData;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newData;
			curr.next = null;
		}
	}
	static int cnt;
	public Number get(int i)
	{
		LinkedList curr;
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		return curr.data;
	}
	public void display()
	{
		if(this.data == null)
			System.out.println("\nList is Empty!");
		else
		{
			LinkedList curr;
			for(curr = this; curr!=null; curr = curr.next)
			{
				curr.data.print();
				System.out.println();
			}
		}
	}
	public LinkedList remove(int i)
	{
		LinkedList curr,prev,head=this;
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		for(prev = this,cnt=0; prev.next!=null && cnt<i-1; prev = prev.next, cnt++);
		if(cnt==0)
		{
			return curr.next;
		}

		else
		{
			prev.next=curr.next;
			return head;
		}
	}
	double temp;
	public double min()
	{
		LinkedList curr=this;
		temp=curr.get(0).getNo();
		for(curr = this; curr.next!=null; curr = curr.next)
		{
			if(temp>curr.data.getNo())
			{
				temp=curr.data.getNo();
			}
		}
		return temp;
	}
	public double max()
	{
		LinkedList curr=this;
		temp=curr.get(0).getNo();
		for(curr = this; curr.next!=null; curr = curr.next)
		{
			if(temp<curr.data.getNo())
			{
				temp=curr.data.getNo();
			}
		}
		return temp;
	}
	public double mean()
	{
		LinkedList curr=this;
		temp=0;
		for(curr = this,cnt=0; curr!=null; curr = curr.next, cnt++)
		{
			temp+=curr.data.getNo();
		}
		return temp/cnt;
	}
	double temp2;
	public double sd()
	{
		LinkedList curr=this;
		double mean=mean();
		temp=0;
		temp2=0;
		for(curr = this,cnt=0; curr!=null; curr = curr.next, cnt++)
		{
			temp=curr.data.getNo();
			temp2+=Math.pow(temp-mean,2);

		}

		return Math.sqrt(temp2/cnt);
	}

}
class Number
{
	double no;
	public double getNo()
	{
		return no;
	}
	public void setNo(double num)
	{
		no=num;
	}
	public Number(double num)
	{
		no=num;
	}
	public Number() {}
	public void print()
	{
		System.out.println(no);
	}
}
