/* Author Smith Christian
date.26.1.2016   */
import java.util.*;
class linkedlist
{
    public int id;
    public String name;
    String add,gender,dob,mob_no;
    linkedlist next;
    public linkedlist(int id,String name,String add,String gender,String dob,String mob_no)
    {
        this.name=name;
        this.id=id;
        this.add=add;
        this.gender=gender;
        this.dob=dob;
        this.mob_no=mob_no;
    }
    public void display()
    {
        System.out.println("Id               "+id);
        System.out.println("Name             "+name);
        System.out.println("Address          "+add);
        System.out.println("Gender           "+gender);
        System.out.println("Date of birth    "+dob);
        System.out.println("Mobile number    "+mob_no);
    }
}
class employee
{
    public linkedlist firstnode,lastnode;
    employee()
    {
        firstnode=null;
        lastnode=null;
    }
    void insertnode(int id,String name,String add,String gender,String dob,String mob_no)
    {
        linkedlist node = new linkedlist(id,name,add,gender,dob,mob_no);
        node.next=null;
        if(firstnode==null)
        {
            firstnode=node;
            lastnode=node;
        }
        else
        {
            lastnode.next=node;
            lastnode=node;
            System.out.println("data inserted successfully");
        }
    }
    void salary(double bs)
    {
        double da,hra,ta,gs,pf,pt,tds,td,ns;
        da=0.6*bs;
        hra=0.12*bs;
        ta=12000;
        gs= bs+da+hra+ta;
        pt=200;
        pf=0.1*bs;
        tds=0.1*(gs-ta-pf-pt);
        td=pf+pt+tds;
        ns=gs-td;
        System.out.println("Gross salary is "+gs);
        System.out.println("Net salary is "+ns);
    }
    void find(String n)
    {
        linkedlist node=firstnode;
        int flag=0;
        do
        {
            String name=node.name;
            if((name.compareTo(n))==0)
            {
                flag=1;
                break;
            }
            else
            {
                node=node.next;
            }
        }while(node!=null);
        if(flag==1)
        {
            node.display();
        }
        else
        {
            System.out.println("Not Found");
        }
    }
    void display()
    {
        linkedlist node=firstnode;
        System.out.println("List of data");
        do
        {
            node.display();
            node=node.next;
        }while(node!=null);
    }
}
class empl
{
    public static void main(String args[])
    {
        employee list = new employee();
        Scanner in = new Scanner(System.in);
        int ch;
        do
        {
            System.out.println("1 : insert\n2 : display\n3 : find\n4 : calculate gross salary and net salary\n5 : exit");
            System.out.println("Enter ur choice: ");
            ch=in.nextInt();
            switch(ch)
            {
                case 1:
                    int id;
                    String name,add,gen,date,mob;
                    System.out.println("Enter your id: ");
                    id=in.nextInt();
                    System.out.println("Enter your name: ");
                    name=in.next();
                    System.out.println("Enter your address: ");
                    add=in.next();
                    System.out.println("Enter your gender: ");
                    gen=in.next();
                    System.out.println("Enter your date of birth: ");
                    date=in.next();
                    do
                    {
                        System.out.println("Enter your mobile number:  ");
                        mob=in.next();
                    }while(mob.length!=10);
                    list.insertnode(id,name,add,gen,date,mob);
                    break;
                case 2:
                    list.display();
                    break;
                case 3:
                    System.out.println("Enter a name to find  ");
                    String n=in.next();
                    list.find(n);
                case 4:
                    System.out.println("Enter ur salary: ");
                    double sal=in.nextDouble();
                    list.salary(sal);
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice!!");
            }
        }while(ch!=5);
    }
}
