/*
author:smith christian
date:19,2,2015
prog matrix 
*/
import java.util.*;

public class matrix {
    int i,j,intr,intc;
    int arr[][];
    public matrix()//constructor
    {
        
        arr = new int[3][3];
        this.intr=3;
        this.intc=3;
    }
    public void read(int r,int c)
    {
       Scanner input = new Scanner(System.in);
      
        int i,j;
    
       for(i=0;i<r;i++)
       { System.out.print("current row\t"+i+"\n");
           for(j=0;j<c;j++)
           { System.out.print("current column\t"+j+"\n");
               arr[i][j]=input.nextInt();
              
           }
       }
    }
    public matrix(int r,int c)//copy constructor
    {
        arr=new int[r][c];
        this.intr=r;
        this.intc=c;
    }
    public void display(int r,int c)
    {
        System.out.println("Value is "+arr[r-1][c-1]);
    }
    public void trans()
    {
        int[][] a = new int[intc][intr];
        for(i=0;i<intc;i++)
        {
            for(j=0;j<intr;j++)
            {
                a[i][j]=arr[j][i];
            }
        }
        for(i=0;i<intc;i++)
        {
            for(j=0;j<intr;j++)
            {
                System.out.print(a[i][j]+"   ");
            }
            System.out.println("\n");
        }
    }
    public String tostring()
    {
        String str = new String();
        for(i=0;i<intr;i++)
        {
            str+="\t";
            for(j=0;j<intc;j++)
            {
                str+=arr[i][j];
                str+="\t";
            }
            str+="\n";
        }
        return str;
    }
    public void add(matrix m)
    {
        int ar=arr.length;
        int ac=arr[0].length;
        int br=m.arr.length;
        int bc=m.arr[0].length;
        if(ar==br && ac==bc)
        {
            int[][] add = new int[ar][ac];
            for(i=0;i<ar;i++)
            {
                for(j=0;j<ac;j++)
                {
                    add[i][j]=arr[i][j]+m.arr[i][j];
                    System.out.print(add[i][j]+"  ");
                }
                System.out.println("\n");
            }
        }
        else
        {
            System.out.println("Addition can't be possible!!!");
        }
    }
    public void multi(matrix m1)
    {
        int ar=arr.length;
        int ac=arr[0].length;
        int br=m1.arr.length;
        int bc=m1.arr[0].length;
        if(ac==br)
        {
            int[][] mul= new int[ar][bc];
            for(i=0;i<ar;i++)
            {
                for(j=0;j<bc;j++)
                {
                    mul[i][j]=0;
                    for(int k=0;k<ac;k++)
                   {
                       mul[i][j]+=arr[i][k]*m1.arr[k][j];
                   }
                    System.out.print(mul[i][j]+"   ");
                }
                System.out.println("\n");
            }
        }
        else
        {
            System.out.println("Multiplication can't be possible");
        }
    }
    public void multi(int c)
    {
        int[][] multi = new int[arr.length][arr[0].length];
        for(i=0;i<arr.length;i++)
        {
            for(j=0;j<arr[0].length;j++)
            {
                multi[i][j]=c*arr[i][j];
                System.out.print(multi[i][j]+"   ");
            }
            System.out.println("\n");
        }
    }
     
    public void magic_square()
    {
      
        int i,j,k,flag=0;
        if(intr==intc)
        {
            int[] a = new int[(intr * 2) + 2];
            for(i=0;i<a.length;i++)
            {
                a[i] = 0;
            }
            for(i = 0 ; i < intr ; i++)
            {
                for(j = 0 ; j < intc ; j++)
                {
                    a[i] += this.arr[i][j];
                }
            }
            for(j = 0 ; j < intc ; j++ , i++)
            {
                for(k = 0 ; k < intr ; k++)
                {
                    a[i] += this.arr[k][j];
                }
            }
            for(k = 0 ; k < intr ; k++)
            {
                a[i] += arr[k][k];
            }
            i++;
            for(k = (intr - 1) , j = 0 ; k >= 0 ; k-- , j++)
            {
                a[i] += arr[k][j];
            }
            for(i = 0 ; (i + 1) < a.length ; i++)
            {
                if(a[i] == a[i + 1])
                {
                    flag++;
                }
            }
            if(flag  == (a.length - 1))
            {
                System.out.println("Matrix is a magic square");
            }
            else
            {
                System.out.println("Matrix is not a magic square");
            }
        }
        else
        {
            System.out.println("Matrix is not a magic square");
        }
    }
    public static void main(String args[])
    {
        Scanner in = new Scanner(System.in);
        int ch;
        matrix m1=new matrix();
        do
        {
           System.out.println("********Menu********");
        System.out.println("(1)assign values for matrix");   
        System.out.println("(2)extract values from the matrix");  
        System.out.println("(3)convert the value of matrix to string");  
        System.out.println("(4)find the tranpose of  matrix");  
        System.out.println("(5)Add two matrices after checking validity of the operation");  
        System.out.println("(6)Multiply two matrices after checking validity of the operation");  
        System.out.println("(7)Multiply a matrix with a scalar value");  
        System.out.println("(8)Check whether a matrix is a magic square");
        System.out.println("(0)EXIT");
            System.out.println("Enter your choice");
            ch=in.nextInt();
            switch(ch)
            {
                case 1:
                    m1.read(3,3);
                    break;
                case 2:
                    System.out.println("Enter NO of row:  ");
                    int row=in.nextInt();
                    System.out.println("Enter NO of column:   ");
                    int column=in.nextInt();
                    m1.display(row,column);
                    break;
                case 3:
                    System.out.println("toString() method   "+m1);
                    break;
                case 4:
                    m1.trans();
                    break;
                case 5:
                    System.out.println("Enter NO of row:  ");
                    int ro=in.nextInt();
                    System.out.println("Enter number of column:   ");
                    int col=in.nextInt();
                    matrix m2=new matrix(ro,col);
                    m2.read(ro,col);
                    m1.add(m2);
                    break;
                case 6:
                    System.out.println("Enter number of row:  ");
                    int r=in.nextInt();
                    System.out.println("Enter number of column:   ");
                    int c=in.nextInt();
                    matrix m3=new matrix(r,c);
                    m3.read(r,c);
                    m1.multi(m3);
                    break;
                case 7:
                  
                    System.out.println("Enter a constant:  ");
                    int con=in.nextInt();
                    m1.multi(con);
                    break;
                case 8:
                    m1.magic_square();
                    break;
                case 0:
                    System.out.print("Exit");
                    break;
                default:
                    System.out.println("invalid choice!!!");
            }
        }while(ch!=0);
    }
}
    