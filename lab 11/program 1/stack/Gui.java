/*
Author smith christian
date 9.3.2016
program stack
*/
package stack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.*;
public class Gui
{
    JButton peep;
    JButton pop;
    JButton push;
     JTextField txtsize,txtpush;
     JLabel lblException ,lblElement; 
    public Gui()
    {   //frame1
        JFrame frame1 = new JFrame();
        frame1.setLayout(new BorderLayout(10,10));
	frame1.setSize(600, 400);
        frame1.setBackground(Color.yellow);
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	frame1.setLocationRelativeTo(null);
	//panel 1 for stack
        JPanel panel = new JPanel();
	panel.setLayout(new FlowLayout(FlowLayout.CENTER));
	panel.setBackground(Color.LIGHT_GRAY);
        // panel 2nd for functions of stack
        JPanel panel1 = new JPanel();
	panel1.setLayout(new GridLayout(4,2));
	panel1.setBackground(Color.GRAY);
         //Panel 2
        JPanel panel2 = new JPanel();
	panel2.setLayout(new GridLayout(5,2));
	panel2.setBackground(Color.LIGHT_GRAY);
        //controls
        push = new JButton("PUSH");
        txtpush = new JTextField();
         pop = new JButton("POP");
         peep = new JButton("Submit Size");
        JLabel lblsize = new JLabel("Size");
         txtsize = new JTextField();
        lblException =  new JLabel("",SwingConstants.CENTER);
        
        lblElement = new JLabel();
         
        JLabel lblStack = new JLabel("Stack Element");
        JLabel l= new JLabel();
        JLabel l1= new JLabel();
        //adding controls in stack
        panel.add(lblStack);
        panel1.add(lblsize);
        panel1.add(txtsize);
        panel1.add(l);
        panel1.add(peep);
        
         panel1.add(push);
        panel1.add(txtpush);
       
        panel1.add(pop);
        
         panel2.add(lblElement);
        panel2.add(lblException);
       //adding panel in frames 
        frame1.add(panel , BorderLayout.NORTH);
          frame1.add(panel2 , BorderLayout.CENTER);
        frame1.add(panel1 , BorderLayout.SOUTH);
        frame1.setVisible(true);
        stackActionListener listener = new stackActionListener(this);
                
        peep.addActionListener(listener);
        push.addActionListener(listener);
        pop.addActionListener(listener);

    }
}
