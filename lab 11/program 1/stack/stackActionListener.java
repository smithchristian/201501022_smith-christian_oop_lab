/*
Author smith christian
date 9.3.2016
program stack
*/
package stack;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import main.stack;

public class stackActionListener implements ActionListener
{
    public Gui obj1;
    public stack s;
    
    public stackActionListener(Gui temp)
    {obj1 = temp;}
    public void actionPerformed(ActionEvent e) 
    {try
        { if(obj1.peep.getText().equals(e.getActionCommand()))
            {
               s = new stack(Integer.parseInt(obj1.txtsize.getText()));
            }
            else if(obj1.push.getText().equals(e.getActionCommand()))
            {
                s.push(Integer.parseInt(obj1.txtpush.getText()));
            }
            else if(obj1.pop.getText().equals(e.getActionCommand()))
            {
                s.pop();
            }
            obj1.lblElement.setText(s.toString());
        }catch(Exception w)
        {  
          obj1.lblException.setText(w.getMessage());
            System.out.println("Error occured");
            obj1.lblException.setVisible(true);
        }finally
        {
            obj1.txtpush.setText("");
            obj1.txtsize.setText("");
        }
    }
}
