/*
Author smith christian
date 9.3.2016
program stack
*/

package linked_list;
import java.util.*;
public class stack extends Exception
{
    int[] stack;
    int cnt,len;
    //Constructor
    public stack(int size)
    {
        this.len=size;
        stack = new int[size];
        cnt = -1;
    }
    //To push a value in stack
    public void push(int num) throws Exception
    {
        if(cnt==len-1)
        {
            System.out.println("Stack is full");
            throw new Exception("Stack is full");
        }
        else
        {
            stack[++cnt]=num;
            System.out.println("Successfully push");
        }
    }
    //To pop a value from stack
    public int pop() throws Exception
    {
        if(cnt<0)
        {
            System.out.println("Stack is empty");
            throw new Exception("Stack is empty");
        }
        else
        {
            System.out.println("Successfully pop");
            return stack[cnt--];
        }
    }
    //To peep a value from top
    void peep()
    {
        if(cnt<0)
        {
            System.out.println("Stack is empty");
        }
        else
        {
            System.out.println("Last member of stack is "+stack[cnt]);
        }
    }
    //To check stack is empty or not
    void empty()
    {
        if(cnt==-1)
        {
            System.out.println("Stack is empty");
        }
        else
        {
            System.out.println("Stack in not empty");
        }
    }
    //To check stack is full or not
    void full()
    {
        if(cnt==len-1)
        {
            System.out.println("Stack is full");
        }
        else
        {
            System.out.println("Stack is not full");
        }
    }
    public String toString() {
		String stackValues = new String();
		
		for(int i = 0; i<=cnt; i++)
			stackValues += stack[i] + " ";
		
		return stackValues;
	}

}
