/* Author  : Smith christian
   Date    : 9/4/2016
   Program : Employee
*/


package Employee;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EActionListener extends Exception implements ActionListener
{
     public Employee_Gui obj1;
     public Employee_main emp =  new Employee_main();
    
    public EActionListener(Employee_Gui temp)
    {
        obj1 = temp;
        
    }
    int cnt=1;
    public void actionPerformed(ActionEvent e) 
    {
        try
        {
            if(obj1.btnfind.getText().equals(e.getActionCommand()))
            {
                for(int i=1;i<obj1.model.getRowCount();i++)
                {
                    if(obj1.txtfind.getText()==obj1.model.getValueAt(i,1).toString())
                    {
                        obj1.txtId.setText(obj1.model.getValueAt(i,0).toString());
                        obj1.txtName.setText(obj1.model.getValueAt(i,1).toString());
                        obj1.txtAdd.setText(obj1.model.getValueAt(i,2).toString());
                        obj1.txtGender.setText(obj1.model.getValueAt(i,3).toString());
                        obj1.txtDob.setText(obj1.model.getValueAt(i,4).toString());
                        obj1.txtContact.setText(obj1.model.getValueAt(i,5).toString());
                    }
                }
                obj1.frame2.setVisible(true);
            }
            else if(obj1.btnsalary.getText().equals(e.getActionCommand()))
            {
                obj1.frame3.setVisible(true);
            }
            else if(obj1.btncount.getText().equals(e.getActionCommand()))
            {
                    double sal = Double.parseDouble(obj1.txtsal.getText());
                    System.out.println(sal);
                    
                     obj1.txtns.setText(Double.toString(emp.getns(sal)));
                     obj1.txtgs.setText(Double.toString(emp.getgs(sal)));
            }            
            else if(obj1.btninsert.getText().equals(e.getActionCommand()))
            {
                obj1.model.insertRow(cnt,new Object[]{obj1.txtid.getText(),obj1.txtname.getText(),obj1.txtadd.getText(),(obj1.rmale.isSelected())?"Male":(obj1.rmale.isSelected())?"Female":"other",obj1.txtdob.getText(),obj1.txtcontact.getText()});
                cnt++;
                obj1.txtid.setText("");
                obj1.txtname.setText("");
                obj1.txtadd.setText("");
                obj1.txtdob.setText("");
                obj1.txtcontact.setText("");
            }
            else if(obj1.btndisplay.getText().equals(e.getActionCommand()))
            {
                obj1.frame4.setVisible(true);
            }
        }
        catch(Exception w)
          {
            System.out.println("ERROR!!!");
        }
    }
    
}
