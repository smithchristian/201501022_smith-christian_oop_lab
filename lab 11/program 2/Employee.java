
/* Author  : Smith christian
   Date    : 9/4/2016
   Program : Employee
*/


package Employee;

/**
 *
 * @author goog
 */



public class Employee
{
    int id;
    String name,add,gender,dob,mob_no;
    double gs=0,ns=0;
    public linkedlist firstnode,lastnode;
    public linkedlist l;
    public Employee(int id,String name,String add,String gender,String dob,String mob_no)
    {
        firstnode=null;
        lastnode=null;
        this.id=id;
        this.name=name;
        this.add=add;
        this.gender=gender;
        this.dob=dob;
        this.mob_no=mob_no;
        insertnode();
    }
    
    public int getid()
    {
        return id;
    }
    public String getname()
    {
        return name;
    }
    public String getadd()
    {
        return add;
    }
    public String getgender()
    {
        return gender;
    }
    public String getdob()
    {
        return dob;
    }
    public String getmob()
    {
        return mob_no;
    }
    public void setid(int eid)
    {
        id=eid;
    }
    public void setname(String ename)
    {
        name=ename;
    }
    public void setadd(String eadd)
    {
        add=eadd;
    }
    public void setgender(String egender)
    {
        gender=egender;
    }
    public void setdob(String edob)
    {
        dob=edob;
    }
    public void setmob(String emob_no)
    {
        mob_no=emob_no;
    }
    void insertnode()
    {
        linkedlist node = new linkedlist(id,name,add,gender,dob,mob_no);
        node.next=null;
        if(firstnode==null)
        {
            firstnode=node;
            lastnode=node;
        }
        else
        {
            lastnode.next=node;
            lastnode=node;
            System.out.println("data inserted successfully");
        }
    }
    public void salary(double bs)
    {
        double da,hra,ta,pf,pt,tds,td;
        da=0.6*bs;
        hra=0.12*bs;
        ta=12000;
        gs= bs+da+hra+ta;
        pt=200;
        pf=0.1*bs;
        tds=0.1*(gs-ta-pf-pt);
        td=pf+pt+tds;
        ns=gs-td;
        System.out.println("Gross salary is "+gs);
        System.out.println("Net salary is "+ns);
    }
    public double getns()
    {
        return ns;
    }
    public double getgs()
    {
        return gs;
    }
    void find(String n)
    {
        linkedlist node=firstnode;
        int flag=0;
        do
        {
            String name=node.name;
            if((name.compareTo(n))==0)
            {
                flag=1;
                break;
            }
            else
            {
                 node=node.next;
            }
        }while(node!=null);
        if(flag==1)
        {
            node.display();
        }
        else
        {
            System.out.println("Not Found");
        }
    }
    void display()
    {
        linkedlist node=firstnode;
        System.out.println("List of data");
        while(node!=null)
        {
            node.display();
            node=node.next;
        }
    }
}
