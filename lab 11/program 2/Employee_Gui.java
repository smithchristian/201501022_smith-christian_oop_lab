/* Author  : Smith christian
   Date    : 9/4/2016
   Program : Employee
*/


package Employee;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.table.*;

public class Employee_Gui
{
    String[]  header = {"Id","Name","Address","Gender","Date of birth","Contact No"};
    String[][] data={{"Id","Name","Address","Gender","Date of birth","Contact No"}};
    public DefaultTableModel model = new DefaultTableModel(data,header);
    public JTable table = new JTable(model);
    JLabel lbl,lblid,lblname,lbladd,lblgender,lbldob,lblcontact;
    JTextField txtid;
    JTextField txtname;
    JTextField txtadd;
    ButtonGroup rgender;
    JRadioButton rmale,rfemale,rother;
   
    JTextField txtdob;
    JTextField txtcontact;
    JButton btninsert;
    JButton btnfind;
    JTextField txtfind;
    JButton btndisplay,btncount; 
    JButton btnsalary;
    JLabel txtId;
    JLabel txtName;
    JLabel txtAdd;
    JLabel txtGender;
    JLabel txtDob;
    JLabel txtContact;
    JFrame frame2 = new JFrame();
    JFrame frame3 = new JFrame();
    JFrame frame4 = new JFrame();
    JLabel lblsal,lblgs,lblns,txtgs,txtns;
    JTextField txtsal;
    public Employee_Gui()
    {
        JFrame frame1 = new JFrame("Employee");
        frame1.setLayout(new BorderLayout());
	frame1.setSize(700, 500);
        frame1.setBackground(Color.RED);
	
        JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(12,2));
	panel.setSize(600, 400);
	panel.setBackground(Color.GRAY);
        
        JPanel panel2 = new JPanel();
        
        lbl = new JLabel("Employee");
        lblid = new JLabel("Id");
        lblname = new JLabel("Name");
        lbladd = new JLabel("Address");
        lblgender = new JLabel("Gender");
        lbldob = new JLabel("Dob");
        lblcontact = new JLabel("Contact no");
        txtid = new JTextField(20);
        txtname = new JTextField(20);
        txtadd = new JTextField(25);
        rgender = new ButtonGroup();
        JComboBox T=new JComboBox();
        T.addItem("Male");
        T.addItem("Female");
        T.addItem("Other");
        rmale = new JRadioButton("Male");
        rfemale = new JRadioButton("Female");
        rother=new JRadioButton("Other");
        rgender.add(rmale);
        rgender.add(rfemale);
        rgender.add(rother);
        txtdob = new JTextField(10);
        txtcontact = new JTextField(10);
        JLabel l = new JLabel();
        btninsert = new JButton("Insert");
        JLabel l1 = new JLabel();
        JLabel l2 = new JLabel();
        JLabel l5 = new JLabel();
        btnfind = new JButton("Find Employee");
        txtfind = new JTextField(20);
        JLabel l3 = new JLabel();
        JLabel l4 = new JLabel();
        btndisplay = new JButton("Display "); 
        btnsalary = new JButton("Count Gross Salary");
       
        panel.add(lblid);
        panel.add(txtid);
        panel.add(lblname);
        panel.add(txtname);
        panel.add(lbladd);
        panel.add(txtadd);
        panel.add(lblgender);
        panel.add(rmale);
        panel.add(rother);
        panel.add(rfemale);
        panel.add(lbldob);
        panel.add(txtdob);
        panel.add(lblcontact);
        panel.add(txtcontact);
        panel.add(l);
        panel.add(btninsert);
        panel.add(l1);
        panel.add(l2);
        panel.add(btnfind);
        panel.add(txtfind);
        panel.add(l3);
        panel.add(l4);
        panel.add(btndisplay);
        panel.add(btnsalary);
        
       panel2.add(lbl);
        
        frame1.setVisible(true);
	
	frame1.add(panel);
        frame1.add(panel2,BorderLayout.NORTH);
        
        
        frame2.setLayout(new BorderLayout());
        frame2.setSize(600, 300);
	frame2.setLocationRelativeTo(null);
	//Panel...
	JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(10,2));
	panel1.setSize(600, 300);
	panel1.setBackground(Color.lightGray);

        JPanel panel3 = new JPanel();
        
        JLabel lbl1 = new JLabel("");
        JLabel lblId = new JLabel("Id");
        txtId = new JLabel(txtid.getText());
        JLabel lblName = new JLabel("Name:    ");
        txtName = new JLabel(txtname.getText());
        JLabel lblAdd = new JLabel("Address:  ");
        txtAdd = new JLabel(txtadd.getText());
        JLabel lblGender = new JLabel("Gender:");
        txtGender = new JLabel((rmale.isSelected())?"Male":((rfemale.isSelected())?"Female":((rother.isSelected())?"Other":null)));
        JLabel lblDob = new JLabel("Dob");
        txtDob = new JLabel(txtdob.getText());
        JLabel lblContact = new JLabel("Contact No");
        txtContact =new JLabel(txtcontact.getText());
        
        panel1.add(lblId);
        panel1.add(txtId);
        panel1.add(lblName);
        panel1.add(txtName);
        panel1.add(lblAdd);
        panel1.add(txtAdd);
        panel1.add(lblGender);
        panel1.add(txtGender);
        panel1.add(lblDob);
        panel1.add(txtDob);
        panel1.add(lblContact);
        panel1.add(txtContact);
        
        panel3.add(lbl1);
        

	frame2.add(panel1);
        frame2.add(panel3,BorderLayout.NORTH);
        
        frame3.setLayout(new GridLayout(4,2));
        frame3.setSize(300, 300);
	frame3.setLocationRelativeTo(null);
        
        JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayout(4,2));
        panel4.setBackground(Color.CYAN);
        
        lblsal = new JLabel("Basic Salary");
        lblns = new JLabel("Net Salary");
        lblgs = new JLabel("Gross Salary");
        txtsal = new JTextField(9);
        txtgs = new JLabel("");
        txtns = new JLabel("");
        JLabel j = new JLabel();
        btncount = new JButton("Count");
        
        panel4.add(lblsal);
        panel4.add(txtsal);
        panel4.add(j);
        panel4.add(btncount);
        panel4.add(lblns);
        panel4.add(txtns);
        panel4.add(lblgs);
        panel4.add(txtgs);
        frame3.add(panel4);
        
              frame4.setLayout(new FlowLayout());
        frame4.setSize(500, 200);
	frame4.setLocationRelativeTo(null);

        JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        table.setVisible(false);
        
       panel5.add(table);
       frame4.add(panel5);
        EActionListener listener = new EActionListener(this);
        btndisplay.addActionListener(listener);
        btnsalary.addActionListener(listener);
        btncount.addActionListener(listener);
        btninsert.addActionListener(listener);
        btnfind.addActionListener(listener);
    
    }
}



