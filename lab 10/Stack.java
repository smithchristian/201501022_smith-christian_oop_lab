/*
authoe smith christian 
rollno 201501022
program Stack
*/package program;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.*;
public class empl
{
    public static void main(String args[])
    {   //frame1
        JFrame frame1 = new JFrame();
        frame1.setLayout(new BorderLayout(10,10));
	frame1.setSize(600, 400);
        frame1.setBackground(Color.yellow);
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	frame1.setLocationRelativeTo(null);
	//panel 1 for stack
        JPanel panel = new JPanel();
	panel.setLayout(new FlowLayout(FlowLayout.CENTER));
	panel.setBackground(Color.LIGHT_GRAY);
        // panel 2nd for functions of stack
        JPanel panel1 = new JPanel();
	panel1.setLayout(new GridLayout(3,2));
	panel1.setBackground(Color.GRAY);
        //controls
        JButton push = new JButton("PUSH");
        JTextField txtpush = new JTextField();
        JButton pop = new JButton("POP");
        JButton peep = new JButton("Peep");
        JLabel lblsize = new JLabel("Size");
        JTextField txtsize = new JTextField();

        JLabel lblStack = new JLabel("Stack Element");
        JLabel lblElement = new JLabel();
        //adding controls in stack
        panel.add(lblStack);
        panel.add(lblElement);
        panel1.add(lblsize);
        panel1.add(txtsize);
        panel1.add(push);
        panel1.add(txtpush);
        panel1.add(pop);
        panel1.add(peep);
   //adding panel in frames 
        frame1.add(panel , BorderLayout.NORTH);
        frame1.add(panel1 , BorderLayout.SOUTH);
        frame1.setVisible(true);
        
        
        
    }
}
