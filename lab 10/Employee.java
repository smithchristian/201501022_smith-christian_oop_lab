
/*
authoe smith christian 
rollno 201501022
program Employee
*/
package program;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Employee {
    public static void main(String[] args) {
        //Frame 1
        JFrame frame1 = new JFrame();
        frame1.setLayout(new GridLayout(2,2));
	frame1.setSize(600, 400);
        frame1.setBackground(Color.yellow);
	
        
          //Frame 2
        JFrame frame2 = new JFrame("Employee detail");
        frame2.setLayout(new GridLayout(2,2));
	frame2.setSize(600, 400);
        frame2.setBackground(Color.yellow);
	frame2.setLocationRelativeTo(frame1); 
        //Frame 3
        JFrame frame3 = new JFrame("Adhoc Employee");
        frame3.setLayout(new GridLayout(2,2));
	frame3.setSize(400, 200);
        frame3.setBackground(Color.yellow);
	      


        	//Panel...
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(8,2));
		panel.setSize(400, 200);
		panel.setBackground(Color.LIGHT_GRAY);
   		//Panel...2 for permanent employee
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(7,2));
		panel1.setSize(200, 200);
		panel1.setBackground(Color.LIGHT_GRAY);
                //Panel... 3 for adhoc employee
		JPanel panel2 = new JPanel();
		panel1.setLayout(new GridLayout(8,2));
		panel1.setSize(200, 200);
		panel1.setBackground(Color.LIGHT_GRAY);
                
                JLabel E=new JLabel("Gender");
                //combo box
                JComboBox T=new JComboBox();
                T.addItem("Male");
                T.addItem("Female");
                // label
                JLabel lblName = new JLabel("Name");
                JLabel lbladdress = new JLabel("address");
		JLabel lblContactNo = new JLabel("Contact Number");
                JLabel lblDOB = new JLabel("Date of Birth (DD/MM/YYYY)");
                JLabel lblDOJ = new JLabel("Enter basic salary");
                
                 JLabel l = new JLabel();
                //textfield
                JTextField txtName = new JTextField(9);
		JTextField txtContactNo = new JTextField(9);
                JTextField txtaddress= new JTextField(9);
                JTextField txtDOB= new JTextField(9);   
                JTextField txtDOJ= new JTextField(9);
                   JTextField txtDOs= new JTextField(9);
                
                //button
                  JButton btnsubmit= new JButton("Exit");    
                  JButton btnAdd= new JButton("ADD");   
                  JButton btnfind= new JButton("FIND");   
                 panel.add(E);
                panel.add(T);
                panel.add(lblName);
                panel.add(txtName);
                panel.add(lbladdress);
                panel.add(txtaddress);        
                panel.add(lblContactNo);
                panel.add(txtContactNo);
                panel.add(lblDOB);
                panel.add(txtDOB);
               panel.add(lblDOJ);
               panel.add(txtDOs);
              
                
            //  panel.add(l);
               
               panel.add(btnfind);
                panel.add(txtDOJ);
                 panel.add(btnAdd);
                 panel.add(btnsubmit);
           //Permanent employee
            JLabel lblDA = new JLabel("Name");
            JLabel lblHRA = new JLabel("Address"); 
            JLabel lblTA = new JLabel("Contact no");
            JLabel lblPT = new JLabel("Dob");
         JLabel lblsalary = new JLabel("Gender");
         JLabel lblsalary1 = new JLabel("Gross Salary");
            JTextField txtDA = new JTextField(9);
            JTextField txtHRA = new JTextField(9);
            JTextField txtTA= new JTextField(9);
            JTextField txtPT= new JTextField(9);
               JTextField txtsalary= new JTextField(9);
               JButton submit1= new JButton("Submit");
            
            panel1.add(lblDA);
           // panel1.add(txtDA);
            panel1.add(lblHRA);
            //panel1.add(txtHRA);
            panel1.add(lblTA);
            //panel1.add(txtTA);
            panel1.add(lblPT);
            //panel1.add(txtPT);
              panel1.add(lblsalary);
            panel1.add(lblsalary1);
            

           //adhoc employee
  JLabel lblPTA = new JLabel("Enter PT (Enter 0 for default value)");
   JTextField txtPTA= new JTextField(9);
    JButton submit2= new JButton("Submit");
            panel2.add(lblPTA);
           panel2.add(txtPTA);
            panel2.add(submit2);
  
        frame1.add(panel);
        frame2.add(panel1);
        frame3.add(panel2);
         frame1.setVisible(true);
            frame2.setVisible(true);
              // frame3.setVisible(true);
    }
}



