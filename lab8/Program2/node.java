 /*
 author-smith christian
 prog-node
 */
 
package lab8;
import oop.*;
import java.util.Scanner;

class node
{
    String name,type,color;
    double price;

    node next;

    node(String name,String color,String type,double price)
    {
        this.name = name;
        this.color = color;
        this.type = type;
        this.price=price;
        next=null;
    }

    void display()
    {
        System.out.println("Customer Name  ->    "+name);
        System.out.println("Vehicle Color  ->    "+color);
        System.out.println("Vehicle Type   ->    "+type);
        System.out.println("Price          ->    "+price);
    }
}

class List
{
    public node fstnode, lastnode;

	List() {
		fstnode = null;
		lastnode = null;
	}
    void insertnode(String name,String color,String type,double price) {
		node n1 = new node(name,color,type,price);
		n1.next = null;
		if(fstnode == null) {
			fstnode = lastnode = n1;
			System.out.println("Linked List CREATED ");
		}
		else {
			lastnode.next = n1;
			lastnode = n1;
			System.out.println("Node INSERTED ");
		}
	}
    void update()
    {
		int count = 0, number, i,a,b;
		node n, n1;
		Scanner input = new Scanner(System.in);

		for(n = fstnode; n != null; n = n.next)
			count++;
		display();
		n = n1 = fstnode;
                String name,color,type;
                double price=0;


		System.out.println(count+" Node's available here!");
	
    System.out.println("Enter the node number which you want to update:");
	
    number =input.nextInt();
    
    
    
    System.out.println("Enter your NAME ");
    
    name=input.next();
    
    System.out.println("Enter COLOR ");
    
    color=input.next();
    
    System.out.println("Enter vehicle type: ");
    
    type=input.next();
    
    System.out.println("Enter your choice1:SELL \n 2:RENT  ");
    
    a=input.nextInt();
    
    if(a==1)
    
    {
    
    System.out.println("Enter YOUR choice(1:Two wheeler  2:Private car   3:Commercial car):  ");
    
    b=input.nextInt();
    
    if(b==1 || b==2 || b==3)
    
    {
    
    System.out.println("Enter price of selling:  ");
    
    price=input.nextDouble();
    
    }
    
    else
    
    {
                        System.out.println("INVALID CHOICE!!!");
    
    }
                }
                else if(a==2)
                {
    
    System.out.println("Enter YOUR choice(1:two wheeler  2:Commercial car):  ");
    
    b=input.nextInt();
    
    if(b==1 || b==2)
                    {
    
    System.out.println("Enter  price of renting:  ");
    
    price=input.nextDouble();
                    }
                    else
                    {
    
    System.out.println("INVALID CHOICE");
                    }
                }
                else
                {
                    System.out.println("INVALID CHOICE");
                }
    
    node n2 = new node(name,color,type,price);
    
    if(number != 1) {
	
    if(number <= count) {
	
    for(i = 2; i <= number; i++)
	
    n = n.next;

				for(i = 2; i <= number-1; i++)
					n1 = n1.next;
                    n1.next = n2;               
                    n2.next = n.next;
        			n.next = null;
		     		n = null;
			}
			else
				System.out.println("Invalid node number!\n");
		}
		else {
                       System.out.println("->HERE<-");

			            fstnode = n2;
                        n2.next = n.next;
                        n = null;
		}

		System.out.println("Node has been DELETED\n");
	}
    	void delete()
        {
		int count = 0, number, i;
		node n, n1;
		Scanner input = new Scanner(System.in);

		for(n = fstnode; n != null; n = n.next)
			count++;
		display();
		n = n1 = fstnode;
		System.out.println(count+" Nodes available here!");
		System.out.println("Enter the node number which you want to delete:");
		number = Integer.parseInt(input.nextLine());
		if(number != 1) {
			if(number <= count) {
				for(i = 2; i <= number; i++)
					n = n.next;

				for(i = 2; i <= number-1; i++)
					n1 = n1.next;

				n1.next = n.next;
				n.next = null;
				n = null;
			}
			else
				System.out.println("INVALID node number!\n");
		}
		else {
			fstnode = n.next;
			n = null;
		}

		System.out.println("Node has been DELETED\n");
    }

    void display()
    {
		node n = fstnode;
		System.out.println("List of NODE:");
		while(n != null) {
			n.display();
			n = n.next;
		}
	}
}

class engine
{
    String en_type;
    Scanner in = new Scanner(System.in);


    public String gettype()
    {
                System.out.print("\nEnter ENGINE type-> ");
        en_type = in.next();
        return en_type;
    }
}
class vehicle
{
    engine e = new engine();

    public String en_type()
    {
        return e.gettype();
    }
}
class TwoWheeler extends vehicle implements sellable,rentable
{
    double sal;
    Scanner in = new Scanner(System.in);
    public double sell_price()
    {
        System.out.println("Enter price of selling two wheeler->  ");
        sal=in.nextDouble();
        return sal;
    }
    public double rent_price()
    {
        System.out.println("Enter rentable price for two wheele->  ");
        sal=in.nextDouble();
        return sal;
    }
    public String gettwovehtype()
    {
      String en_ty;
      en_ty = en_type();
      return en_ty;
    }
}
abstract class FourWheeler extends vehicle
{
    double sal;
    Scanner in = new Scanner(System.in);
    public abstract String get_veh_type();
}
class PrivateCar extends FourWheeler implements sellable
{
       public double sell_price()
    {
        System.out.println("Enter price of selling  two wheeler->  ");
        sal=in.nextDouble();
        return sal;
    }
    public String get_veh_type()
    {
        String a="private vehicle";
        return a;
    }
}
class CommercialCar extends FourWheeler implements sellable,rentable
{
    public double sell_price()
    {
        System.out.println("Enter selling price for two wheeler->  ");
        sal=in.nextDouble();
        return sal;
    }
    public double rent_price()
    {
        System.out.println("Enter rentable price for two wheeler->  ");
        sal=in.nextDouble();
        return sal;
    }
    public String get_veh_type()
    {
        String a="Commercial vehicle";
        return a;
    }
}
class MyVehicle
{
    public static void main(String[] args)
    {
        int choice,ch,c,select,a;
        double price;
        String name,color;
        Scanner sc = new Scanner(System.in);
        List ll = new List();
        do
        {
            System.out.println("\t\tMENU\t\t");
            System.out.println("1 : CREATE");
            System.out.println("2 : UPDATE");
            System.out.println("3 : DELETE");
            System.out.println("4 : DISPLAY");
            System.out.println("0 : Exit");
            System.out.println("\nEnter your Choice: ");
            choice = sc.nextInt();
            switch(choice)
            {
                case 1:     //Create
                    System.out.println("Enter your name:  ");
                    name = sc.next();
                    System.out.println("Enter color of vehicle:  ");
                    color = sc.next();
                    if(choice == 1)
                    {
                        System.out.println("\tVehicle Type");
                        System.out.println("1 : Two Wheeler");
                        System.out.println("2 : Four Wheeler");
                        System.out.println("\nEnter vehicle type: ");
                        ch = sc.nextInt();
                        if(ch == 1)
                        {
                          TwoWheeler v1 = new TwoWheeler();
                          System.out.println("Enter your choice(1:sell 2:rent):  ");
                          a=sc.nextInt();
                          if(a==1)
                          {
                              System.out.println("Enter sellable price:  ");
                          }
                          else if(a==2)
                          {
                              System.out.println("Enter rentable price:  ");
                          }
                          price=sc.nextDouble();
                          ll.insertnode(name,color,v1.gettwovehtype(),price);
                        }
                        else if(ch == 2)
                        {
                            System.out.println("\t\tFour Wheeler Type");
                            System.out.println("1 : Private Car");
                            System.out.println("2 : Commercial Car");
                            System.out.println("\nEnter four wheeler type: ");
                            c = sc.nextInt();
                            if(c == 1)
                            {
                                FourWheeler v1 = new PrivateCar();
                                System.out.println("Enter sellable price:  ");
                                price = sc.nextDouble();
                                ll.insertnode(name,color,v1.get_veh_type(),price);
                            }
                            else
                            {
                                FourWheeler v1 = new CommercialCar();
                                System.out.println("Enter your choice(1:sell 2:rent):  ");
                                a=sc.nextInt();
                                if(a==1)
                                {
                                    System.out.println("Enter sellable price:  ");
                                }
                                else if(a==2)
                                {
                                    System.out.println("Enter rentable price:  ");
                                }
                                price=sc.nextDouble();
                                ll.insertnode(name,color,v1.get_veh_type(),price);
                            }
                        }
                    }
                    ll.display();
                    break;
                case 2:     //Update
                    ll.update();
                    ll.display();
                    break;
                case 3:     //Delete
                    ll.delete();
                    ll.display();
                    break;
                case 4:     //Display
                    ll.display();
                case 0:     //Exit
                    break;
                default:
                    System.out.println("INVAlID Choice!!!");
                   break;
            }
        }while(choice != 0);

    }
}
