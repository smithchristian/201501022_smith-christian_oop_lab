/*
* Program ->BANKING
* Author: smith christian
* Date: Februray 24,2016
*/



import java.util.*;

abstract class Account
{
    private String acc_no;
    protected double balance;
    String Acc_DateofCreation = new String();


    Account(String no,double bal ,String date)
    {
        acc_no = no;
        balance = bal;
        Acc_DateofCreation = date;
    }

    public String getAccountNo()
    {
        return acc_no;
    }

    public boolean deposit(double amt)
    {
        if(amt > 0)
        {
            balance += amt;
            return true;
        }
        else
        {
            return false;
        }
    }

    public abstract double getBalance();
    public abstract boolean withdraw(double amt);
}


class Savings extends Account
{
    private double roi;

    int Acc_DateofCreation_year,Acc_DateofCreation_month,Acc_DateofCreation_day;

    GregorianCalendar Acc_LastUpdate=new GregorianCalendar(Acc_DateofCreation_year,Acc_DateofCreation_month,Acc_DateofCreation_day);


    public Savings(String no,double bal,String date,double rate)
    {
        super(no,bal,date);

        Acc_DateofCreation_day=Integer.parseInt(Acc_DateofCreation.substring(0,2));
        Acc_DateofCreation_month=Integer.parseInt(Acc_DateofCreation.substring(3,5));
        Acc_DateofCreation_year=Integer.parseInt(Acc_DateofCreation.substring(6,10));

        if(rate>0)
        {
            roi = rate;
        }
        else
        {
        	System.out.println("Invalid Entry!!");
        }
    }

    public boolean setRoi(float rate)
    {
        if(rate > 0)
        {
            roi = rate;
            return true;
        }
        else
        {
            return false;
        }
    }

    public double getRoi()
    {
        return roi;
    }

    public boolean withdraw(double amt)
    {
        if(amt > 0 && amt<=balance)
        {
        	double tempBal=balance;
        	tempBal-=amt;

        	if(tempBal>=500)
        	{
        		balance = tempBal;
        		return true;
        	}
        	else
        	{
        		System.out.println("\nYour remaining balance cannot be less than 500.");
        		return false;
        	}
        }
        else
        {
            return false;
        }
    }

    public double getBalance()
    {
    	Calendar now = Calendar.getInstance();

        int Acc_DateofJoining_day,Acc_DateofJoining_month,Acc_DateofJoining_year;

        Acc_DateofJoining_day=Integer.parseInt(Acc_DateofCreation.substring(0,2));
        Acc_DateofJoining_month=Integer.parseInt(Acc_DateofCreation.substring(3,5));
        Acc_DateofJoining_year=Integer.parseInt(Acc_DateofCreation.substring(6,10));


    	GregorianCalendar joining =new GregorianCalendar(Acc_DateofJoining_year,Acc_DateofJoining_month,Acc_DateofJoining_day);
    	if(Acc_LastUpdate.getTimeInMillis()==joining.getTimeInMillis())
    	{
	    	GregorianCalendar curr_date =new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)-1, now.get(Calendar.DATE));

	    	long days = (curr_date.getTimeInMillis()-joining.getTimeInMillis())/(1000*60*60*24);
                balance*=Math.pow((1+(roi/400)),days/90);
	    	Acc_LastUpdate.setTimeInMillis(curr_date.getTimeInMillis());
    	}

    	return balance;
    }
}


class Current extends Account
{
    private double odl;

    public Current(String no,double bal,String date,double odl)
    {
        super(no,bal,date);
        if(odl>=500 && odl<=10000000)
        {
            this.odl=odl;
        }
        else
        {
            System.out.println("Over Draft Limit should be between 500 to 10000000(1 crore) ");
        }
    }

    public boolean setOdl(double amt)
    {
        if(amt > 0)
        {
            odl = amt;
            return true;
        }
        else
        {
            return false;
        }
    }

    public double getOdl()
    {
        return odl;
    }

    public boolean withdraw(double amt)
    {
        if(amt>0 && amt<balance+odl)
        {
        	balance -= amt;
            return true;
        }
        else
        {
            return false;
        }
    }

    public double getBalance()
    {
        return balance;
    }
}


public class Banking {




        public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int acc_type,choice;
        double amount;
        String Creation_date=new String();
        Account a1;

        System.out.print("Create an Account :");
        System.out.print("\n1. Savings Account ");
        System.out.print("\n2. Currentaccount");

        System.out.print("\nEnter ur choice: ");
        acc_type = input.nextInt();

        if(acc_type!=1 && acc_type!=2)
        {
        	System.out.println("Select your choice !");
        	return;
        }

        System.out.println("Enter the date of creating the account (of the form DD-MM-YYYY) : ");
        Creation_date=input.nextLine();
        Creation_date=input.nextLine();

        if(acc_type==1)
        {
            System.out.println("Enter ROI : ");
            a1 = new Savings("079254618221",1000,Creation_date,input.nextDouble());
        }
        else
        {
            System.out.println("Enter Over Draft Limit : ");
            a1 = new Current("079254618222",1000,Creation_date,input.nextDouble());
        }

        while(true)
        {
            System.out.println("select your choice ?");
            System.out.println("1. Check current Balance");
            System.out.println("2. Deposit your money");
            System.out.println("3. Withdraw your money");
            System.out.println("4. EXIT");

            System.out.println("\nEnter ur choice..: ");
            choice = input.nextInt();

            switch(choice)
            {
                case 1: //Check Balance
                    System.out.println("\nAccount No : " + a1.getAccountNo());
                    if(a1 instanceof Savings)
                    {
                    	Savings temp=(Savings)a1 ;
                    	System.out.println("\nBalance : " + temp.getBalance());
                    }



                    else
                    {
                    	Current temp=(Current)a1;
                    	System.out.println("\nBalance : " + temp.getBalance());
                    }
                    break;

                case 2: //Deposit
                    System.out.println("\nEnter the Amount want to deposit ....: ");
                    amount = input.nextDouble();

                    if(a1.deposit(amount))
                    {
                        System.out.println("\nDeposit is done!!!!");
                        System.out.println("\nAccount No. : " + a1.getAccountNo());
                        System.out.println("\nBalance : " + a1.getBalance());
                    }
                    else
                    {
                        System.out.println("\nDeposit is not done!");
                    }
                    break;

                case 3: //Withdraw
                    System.out.println("\nEnter the amount wanted to withdraw : ");
                    amount = input.nextDouble();

                    if(a1.withdraw(amount))
                    {
                        System.out.println("Withdraw is done!");
                        System.out.println("Account No. : " + a1.getAccountNo());
                        System.out.println("Balance : " + a1.getBalance());
                    }
                    else
                    {
                        System.out.println("\nWithdraw Unsuccessful!");
                    }
                    break;

                case 4:  //Exit
                    break;

                default:
                    System.out.println("WRONG CHOICE !!!!!");
                    break;
            }
        }
    }
}
