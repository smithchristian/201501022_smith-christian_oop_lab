/*
Program:queue

Date:22/2/16

Author:smith christian

*/

import java.util.Scanner;

class LinearQueue
{
    protected int[] Queue;
    protected int first, last,Qsize, QueueLength;
    
    public LinearQueue(int n)
    {
       
        Qsize = n;
        
        
        
        QueueLength = 0;
        
        Queue = new int[Qsize];
        
        first = -1;
        
        
        
        last = -1;
    }
    
    public boolean Empty()
    {
        return first == -1;
    }
    
    
    
    
    public boolean Full()
    {
        return first == 0 && last == Qsize - 1; 
    }
    
    public int Size()
    {
        return QueueLength;
    }
    
    public void queue(int i)
    {
    
        if(last == -1)
        {
            first = 0;
            last = 0;
            Queue[last] = i;
        }
        
        
        
        else if(last + 1 >= Qsize)
        {
            System.out.println("Queue is Full.\nEnqueuing not Possible!!!!!!!!!!!");
        }
        
        
        
        else if(last + 1 < Qsize)
        {
            Queue[++last] = i;
            
        }
        QueueLength++;
    }
    
    
    
    
    public int Dequeue()
    {
        if(Empty())
        {
            System.out.println("Queue is Empty!");
            return 0;
        }
        else
        {
            QueueLength--;
            
            int element = Queue[first];
            
            if(first == last)
            {
                first = -1;
                last = -1;
            }
            else
                first++;
            
            return element;
        }
    }
    
    public void displayQueue()
    {
        int i;
        
        System.out.print("Queue = ");
        
        if(QueueLength == 0)
        {
            System.out.print("EMPTY!");
            return;
        }
        
        System.out.print(Queue[first] + " ");
        for(i = first + 1; i <= last; i++)
            System.out.print(Queue[i] + " ");
        
        System.out.println();
        
    }
}

class CircularQueue
{
    protected int[] CQueue;
    protected int first, last, Qsize, QueueLength;
    
    public CircularQueue(int n)
    {
        Qsize = n;
        
        CQueue = new int[n];
        
        first =last = -1;
    }
    
    public void Enqueue(int x)
    {
        if(last!=Qsize-1)
        {
            if((last + 1)%Qsize != first)
            {
                last = (last + 1)%Qsize;
                CQueue[last] = x;
            }
        }
        else
            System.out.println("Queue is FULL!!!!!!!");
    }
    
    public int Dequeue()
    {
        int x;
        
        if(first != last)
        {
            first = (first + 1)%Qsize;
            x = CQueue[first];
            return x;
        }
        else if(first==last)
        {
            first=-1;
            last=-1;
            return CQueue[0];
        }
        else
            return -9999;
    }
    
    public void displayQueue()
    {
        
        int i;
        
        System.out.print("Queue = ");
        if(first<last)
        {
            for(i=first;i<last;i++)
            {
                System.out.print(CQueue[i+1] + " ");
            }
        }
        else if(first>last)
        {
            for(i=first;i<Qsize;i++)
            {
                System.out.print(CQueue[i] + " ");
            }
            for(i=0;i<=last;i++)
            {
                System.out.print(CQueue[i] + " ");
            }
        }
        else if(first == 0&&last==0)
        {
            System.out.print(CQueue[first] + " ");
        }
        else
        {
            System.out.println("the queue is empty");
        }
    }
   }

public class Queue 
{
    public static void main(String[] args) 
    {
        int n, choice, k, ch, opt;
        
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("enter the size of queue: ");
        n = input.nextInt();
        
        System.out.println("(1) linear search.");
        System.out.println("(2) circular search");
        opt = input.nextInt();
        
        if(opt == 1)
        {
            LinearQueue lq = new LinearQueue(n);

            do
            {
                System.out.println("\n (1). enqueue an Element.....");
                System.out.println("(2). dequeue an Element.....");
                System.out.println("(3). Check whether the List is Empty.");
                System.out.println("(4). Check whether the List is Full.");
                System.out.println("(0). Exit.");
                System.out.println("enter your choice: ");
                choice = input.nextInt();

                switch(choice)
                {
                
                    case 1:
                            System.out.println("Enter the element's  to Enqueue: ");
                            k = input.nextInt();

                            lq.queue(k);
                            break;
                    
                    case 2:
                            System.out.println("Dequeued Element are: " + lq.Dequeue());
                            break;
                    
                    case 3:
                            System.out.println("empty Status = " + lq.Empty());
                            break;
                    
                    case 4:
                            System.out.println("full Status = " + lq.Full());
                            break;
                    
                    case 0:
                            default: System.out.println("Wrong choice!");
                            return;
                
                }

                lq.displayQueue();

                System.out.println("do you want yo continue?(Press 1 for YES. Press 0 for NO.)");
                ch = input.nextInt();

            }
            
            
            
            while(ch == 1);
        
        }
        
        else if(opt == 2)
        
        {
        
            CircularQueue cq = new CircularQueue(n);
            
            
            do
            
            {
            
                
                System.out.println("\n1. Enqueue an Element.");
                System.out.println("2. Dequeue an Element.");
                System.out.println("0. Exit.");
                System.out.println("enter ur choice: ");
                choice = input.nextInt();
                
                switch(choice)
                {
                    case 1:
                            System.out.println("enter the  element to enqueue: ");
                            k = input.nextInt();
                            
                            cq.Enqueue(k);
                            break;
                    case 2:
                            int popped;
                            popped = cq.Dequeue();
                            System.out.print(popped);
                            break;
                    case 0:
                            default: System.out.println("WRONG CHOICE!");
                            return;
                }
                
                cq.displayQueue();

                System.out.println("\nDo you want to continue?(Press 1 for Yes. Press 0 for No.)");
                ch = input.nextInt();
            }
            while(ch == 1);
        }
    }
}
