/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9;

/**
 *
 * @author goog
 */
/*
author:smith christian
date:19,2,2015
prog vector 
*/
import java.util.*;
import java.lang.Math.*;

public class vector 
{    
    int i;
    static int siz;
    static int[] vect;
    static int[] c;
    vector(int x)//constructor
    {//Exception handling 
        // dimension of vector cannot be negative
        
       try{ 
           this.siz=x;   
        vect = new int[x];
        if (x<0){
        }
        
        
    }catch(Exception e){
        System.out.println("INVALID CHOICE");
    }
    }
    
    public int get()
    {
        return siz;
    }
    public void read()//function to read the value of vector
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the element's of vector:  ");
        for(i=0;i<vect.length;i++)
        {
            System.out.print("current position "+i+"\n");
            vect[i]=in.nextInt();
        }
    }
    public void display(int[] a)// function to display vector
    {
        System.out.println("(");
        for(i=0;i<a.length;i++)
        {
            if(i<(a.length-1))
            {
                System.out.print(a[i]+",");
            }
            else
            {
                System.out.println(a[i]+")");
            }
        }
    }
    public void display()// function to display vector
    {
        System.out.print("(");
        for(i=0;i<vect.length;i++)
        {
            if(i<(vect.length-1))
            {
                System.out.print(vect[i]+",");
            }
            else
            {
                System.out.println(vect[i]+")");
            }
        }
    }

    public void magnitude()//function to calc the magnitude of vector
    {
        float magni=0;
        for(i=0;i<vect.length;i++)
        {
            magni+=vect[i]*vect[i];
        }
        System.out.println("Magnitude is "+(float)Math.sqrt(magni));
    }
    public void addition(vector v)//function to calc the addition of two vector
    {
        c = new int[vect.length];
        if(vect.length==v.vect.length)
        {
            for(i=0;i<vect.length;i++)
            {
                c[i]=vect[i]+v.vect[i];
            }
            display(c);
        }
        else
        {
            System.out.println("operation is not  possible:x:x:x");
        }
    }
    public void sub(vector v)//function to calc the subtraction of two vector
    {
        c = new int[vect.length];
        if(vect.length==v.vect.length)
        {
            for(i=0;i<vect.length;i++)
            {
                c[i]=vect[i]-v.vect[i];
            }
            display(c);
        }
        else
        {
            System.out.println("operation  is not  possible:x:x");
        }
    }
    public void product(vector v)  //function to calc the multipilication of two vector
    {
        int sum=0;
        for(i=0;i<vect.length;i++)
        {
            sum+=vect[i]*v.vect[i];
        }
        System.out.println("Multiplication of two vector is  "+sum);
    }
    public void c_product(vector v)//function to multiply by a constant
    {
        c[0]=vect[1]*v.vect[2]-vect[2]*v.vect[1];
        c[1]=vect[2]*v.vect[0]-vect[0]*v.vect[2];
        c[2]=vect[0]*v.vect[1]-vect[1]*v.vect[0];
        display(c);
    }
    public void multi(int b)
    {
        c= new int[vect.length];
        for(i=0;i<vect.length;i++)
        {
            c[i]=b*vect[i];
        }
        display(c);
    }
    public void polar()//function to convert to polar coordinate
    {
        double x1,x2,x3,r,theta ,fi;		
					
		Scanner in = new Scanner(System.in);	
		if(this.siz == 2)
		{
			System.out.println("Enter magnitude of position vactor : ");
			r=in.nextInt();
			System.out.println("Enter angle between position vector and x-axis: ");
			theta=in.nextDouble();
			
			x1=r*Math.cos(Math.toRadians(theta));
			x2=r*Math.sin(Math.toRadians(theta));
			
			System.out.println("x  coordinate: "+x1);
			System.out.println("y  coordinate: "+x2);	
 		}
		else if(this.siz == 3)
		{
			System.out.println("Enter magnitude of position vactor : ");
			r=in.nextInt();
			System.out.println("Enter angle btn position vector and z-axis: ");
			theta=in.nextDouble();
			System.out.println("Enter angle btn component of position vector in x-y plane and x-axis : ");
			fi=in.nextDouble();
			
			x1 = r * Math.sin(Math.toRadians(theta)) * Math.cos(Math.toRadians(fi));
			x2 = r * Math.sin(Math.toRadians(theta)) * Math.sin(Math.toRadians(fi));
			x3 = x1 = r * Math.cos(Math.toRadians(theta));
				
			System.out.println("\nX - Coordinate : " + x1);
			System.out.println("\nY - Coordinate : " + x2);
			System.out.println("\nZ - Coordinate : " + x3);
			
		}
		else
		{
			System.out.println("\nCannot find Polar co-ordinates of this vector");
		}
    }
    public static void main(String args[]) throws Exception
    {
        Scanner in=new Scanner(System.in);
        int ch,dim;
        System.out.println("Enter the dimensions of the vector.  ");
        dim=in.nextInt();
        vector v1=new vector(dim);
        //checking for dim >0
        while(dim<0)
        {
               System.out.println("Enter the dimensions of the vector.  ");
        dim=in.nextInt();
               try{ 
           
        vect = new int[dim];
    }catch(Exception e){
        System.out.println("INVALID CHOICE");
    }
     
        }
        do
        {
            System.out.println("*******MENU*******");
            System.out.println("1 : Insert the  value to the  vector");
            System.out.println("2 : Extract the Value to the  vector");
            System.out.println("3 : Find the Magnitude of the a Vector");
            System.out.println("4 : Add two Vectors after checking the validity of the addition");
            System.out.println("5 : Subtract two Vectors after checking the validity of the subtraction");
            System.out.println("6 : Find Scalar product of two Vectors after checking validity of the scaler product");
            System.out.println("7 : Find Vector product of two Vectors after checking validity of the vector product");
            System.out.println("8 : Multiply a Vector by a constant");
            System.out.println("9 : Calculate Polar Co-ordinates of the Vector");
            System.out.println("0 : Exit");
            System.out.println("Enter your choice:  ");
            ch=in.nextInt();
            switch(ch)
            {
                case 1:
                    v1.read();
                    break;
                case 2:
                    v1.display();
                    break;
                case 3:
                    v1.magnitude();
                    break;
                case 4:
                    System.out.println("Enter dimension of a vector:  ");
                    dim=in.nextInt();
                    if(dim==v1.get())
                    {
                        vector v2 = new vector(dim);
                        v2.read();
                        v2.addition(v1);
                    }
                    else
                    {
                        System.out.println("Vectors can't be added");
                    }
                    break;
                case 5:
                    System.out.println("Enter dimension of a vector:  ");
                    dim=in.nextInt();
                    if(dim==v1.get())
                    {
                        vector v3 = new vector(dim);
                        v3.read();
                        v3.addition(v1);
                    }
                    else
                    {
                        System.out.println("Vectors can't be substracted");
                    }
                    break;
                case 6:
                    System.out.println("Enter dimension of a vector:  ");
                    dim=in.nextInt();
                    if(dim==v1.get())
                    {
                        vector v4 = new vector(dim);
                        v4.read();
                        v4.product(v1);
                    }
                    else
                    {
                         System.out.println("operation is not possible");
                    }
                    break;
                case 7:
                    System.out.println("Enter dimension of a vector:  ");
                    dim=in.nextInt();
                    if(v1.get()==3)
                    {
                        vector v5 = new vector(dim);
                        v5.read();
                        v5.c_product(v1);
                    }
                    else
                    {
                        System.out.println("operation  is not posssible");
                    }
                    break;
                case 8:
                    System.out.println("Enter a constant: ");
                    int n=in.nextInt();
                    v1.multi(n);
                    break;
                case 9:
                    v1.polar();
                    break;
                case 0:
                    System.out.print("EXIT");
                    break;
                default:
                    System.out.println("INVALID CHOICE");
            }
        }while(ch!=0);
    }
}