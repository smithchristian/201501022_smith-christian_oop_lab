/*Author smith christian
  date 12.2.2016
  program Polynomial
*/

package oop;
public class node
{
    public int coef,exp;
    public node next;
    public node(int c,int e)        //constructor
    {
        coef=c;
        exp=e;
        next=null;
    }
    public void display()
    {
        System.out.println(coef+"x^"+exp+"   ");
    }
}
