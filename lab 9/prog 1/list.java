/*Author smith christian
  date 12.2.2016
  program Polynomial
*/

package oop;
public class list
{
    public node curr1,curr2,first=null;
    public int co,ex;
    public void insert(int c,int e)        //to insert
    {
        node n1=new node(c,e);
        node curr;
        curr=first;
        if(curr==null)
        {
            first=n1;
        }
        else
        {
            n1.next=first;
            first=n1;
        }
    }
    public void display()        //to display
    {
        node curr;
        curr=first;
        while(curr!=null)
        {
            curr.display();
            curr=curr.next;
        }
    }
    public void addition(list l1,list l2)        //addition of polynomials
    {
        try
        {
            list l3=new list();
            curr1=l1.first;
            curr2=l2.first;
            while(curr1!=null && curr2!=null)
            {
                co=curr1.coef+curr2.coef;
                ex=curr1.exp;
                l3.insert(co,ex);
                curr1=curr1.next;
                curr2=curr2.next;
            }
            System.out.println("\nAddition of two polynomials is   ");
            l3.display();
        }
        catch(Exception e)
        {
            System.out.println("Addition is not possible");
        }
    }
    public void subtract(list l1,list l2)        //subtraction of polynomial
    {
        
        list l4=new list();
        curr1=l1.first;
        curr2=l2.first;
        while(curr1!=null && curr2!=null)
        {
            co=curr1.coef-curr2.coef;
            ex=curr1.exp;
            l4.insert(co,ex);
            curr1=curr1.next;
            curr2=curr2.next;
        }
        System.out.println("\nSubtraction of two polynomials:   ");
        l4.display();
    }
    public void multi(list l1,list l2)        //multiplication of two polynomial
    {
        node curr3;
        list l5 = new list();
        curr1=l1.first;
        curr2=l2.first;
        while(curr1!=null)
        {
            while(curr2!=null)
            {
                l5.insert((curr1.coef*curr2.coef),(curr1.exp+curr2.exp));
                curr2=curr2.next;
            }
            curr2=l2.first;
            curr1=curr1.next;
        }
        curr1=l5.first;
        while(curr1!=null)
        {
            curr3=curr1;
            curr2=curr2.next;
            while(curr2!=null)
            {
                if(curr1.exp==curr2.exp)
                {
                    curr1.coef+=curr2.coef;
                    curr3.next=curr2.next;
                }
                curr3=curr3.next;
                curr2=curr3.next;
            }
            curr1=curr1.next;
        }
        System.out.println("\nMultiplication of two Polynomials:  ");
        l5.display();
    }
    public void mul_c(list l1,int cons)        //multiplication with constant
    {
        list l5=new list();
        curr1=l1.first;
        while(curr1!=null)
        {
            curr1.coef=curr1.coef*cons;
            ex=curr1.exp;
            l5.insert(curr1.coef,ex);
            curr1=curr1.next;
        }
        System.out.println("\nPolynomial after multiplying with constant---> "+cons+"   ");
        l5.display();
    }
}