/*Author smith christian
  date 12.2.2016
  program Polynomial
*/
package lab9;
import java.util.Scanner;
import oop.*;
class polinomial
{
    public static void main(String args[])
    {
        int ch,degree=0,cons,i,c;
        list l1=new list();
        list l2=new list();
        Scanner input=new Scanner(System.in);
        do
        {
            System.out.println("\t\tMENU\t\t");
            System.out.println("1 : Enter Polynomials");
            System.out.println("2 : Display Polynomials");
            System.out.println("3 : Addition of two Polynomials");
            System.out.println("4 : Subtraction of two Polynomials");
            System.out.println("5 : Multiplication of two Polynomials");
            System.out.println("6 : Multiplication with constant value");
            System.out.println("0 : Exit");
            System.out.println("Enter ur choice: ");
            ch=input.nextInt();
            switch(ch)
            {
                case 1:
                    try
                    {
                        System.out.println("Enter the polynomial degree:  ");
                        degree=input.nextInt();
                        if(degree<0)
                            throw new Exception("Invalid input");
                    }
                    catch(Exception e)
                    {
                        System.out.println("Negative degree is not valid!!!");
                    }
                    System.out.println("\nFirst Polynomial:-  ");
                    for(i=degree;i>=0;i--)
                    {
                        System.out.println("Enter coefficiant of x^"+i+":  ");
                        c=input.nextInt();
                        l1.insert(c,i);
                    }
                    System.out.println("\nSecond Polynomial:-  ");
                    for(i=degree;i>=0;i--)
                    {
                        System.out.println("Enter coefficiant of x^"+i+":  ");
                        c=input.nextInt();
                        l2.insert(c,i);
                    }
                    break;
                case 2:
                    System.out.println("First Polynomial:  ");
                    l1.display();
                    System.out.println("Second Polynomial:  ");
                    l2.display();
                    break;
                case 3:
                    list p1 = new list();
                    p1.addition(l1,l2);
                    break;
                case 4:
                    list p2 = new list();
                    p2.subtract(l1,l2);
                    break;
                case 5:
                    list p3 = new list();
                    p3.multi(l1,l2);
                    break;
                case 6:
                    System.out.println("Enter a constant value:  ");
                    cons=input.nextInt();
                    list p4 = new list();
                    p4.mul_c(l1,cons);
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Wrong Choice!!!");
                    break;
            }
        }while(ch!=0);
    }
}
