/*Author smith christian
  date 12.2.2016
  program Polynomial
*/

package linked_list;
public class Current extends Account
{
    private double odl;    
    public Current(String no,double bal,String date,double odl)        //constructor
    {
        super(no,bal,date);
        if(odl>=500 && odl<=10000000)
        {
            this.odl=odl;
        }
        else
        {
            System.out.println("limit of OverDraft should be between 500 to 1 crore");
        }
    }
    
    public boolean setOdl(double amt)
    {
        if(amt > 0)
        {
            odl = amt;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public double getOdl()
    {
        return odl;
    }
    
    public void withdraw(double amt) throws MyException        //withdraw amount
    {
        try
        {
            if(amt <= 0)
                throw new MyException("Positive Amout Required!");
            else if(amt > balance)
                throw new MyException("Insufficient Balance!");
            else
                balance -= amt;
        }
        catch(MyException err)
        {
            throw err;
        }
    }
    
    public double getBalance()
    {
        return balance;
    }
}