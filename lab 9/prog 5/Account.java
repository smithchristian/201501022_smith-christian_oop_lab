/*Author smith christian
  date 12.2.2016
  program Polynomial
*/

package linked_list;
public abstract class Account        //abstarct class
{
    private String acc_no;
    protected double balance;
    String Acc_DateofCreation = new String();
    
    
    public Account(String no,double bal ,String date)        //constructor
    {
        acc_no = no;
        balance = bal;
        Acc_DateofCreation = date;
    }
    
    public String getAccountNo()        //get account number
    {
        return acc_no;
    }
    
    public void deposit(double amt) throws MyException        //To deposit amount
    {
        try
        {
            if(amt >= 0)
            {
                balance += amt;

            }
            else if(amt<0)
            {
                throw new MyException("Can not deposit negative amount");
            }
        }
        catch(MyException e)
        {
            throw e;
        }
    }
    
    public abstract double getBalance();
    public abstract void withdraw(double amt)  throws MyException;
}