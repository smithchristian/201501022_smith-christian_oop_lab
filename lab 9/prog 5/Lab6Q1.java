/*Author smith christian
  date 12.2.2016
  program  Bank
*/

package array;
import java.util.*;
import linked_list.*;

public class Lab6Q1 
{    
    public static void main(String[] args) 
    {	
        Scanner input = new Scanner(System.in);
        int acc_type,ch;
        double am;
        String Creation_date=new String();
        Account a1;
        
        System.out.println("Create Account-> ");
        System.out.println("1 -> Savings");
        System.out.println("2 -> Current");
        System.out.println("Enter ur choice: ");
        acc_type = input.nextInt();
        
        if(acc_type!=1 && acc_type!=2)
        {
        	System.out.println("Wrong choice!!!");
        	return;
        }
        
        System.out.println("Enter Account Creation Date(of the form DD-MM-YYYY): ");
        Creation_date=input.nextLine();
        Creation_date=input.nextLine();
        
        if(acc_type==1)
        {
            System.out.println("Enter rate of interest -> ");
            a1 = new Savings("079112300001",1000,Creation_date,input.nextDouble());
        }
        else
        {
            System.out.println("Enter over draft limit -> ");
            a1 = new Current("079112300002",1000,Creation_date,input.nextDouble());
        }
       
        do
        {
            System.out.println("What would you like to do ?");
            System.out.println("1 -> To Check Balance");
            System.out.println("2 -> To Deposit");
            System.out.println("3 -> To Withdraw");
            System.out.println("0 -> Exit");
            System.out.println("Enter your choice-> ");
            ch = input.nextInt();
        
            switch(ch)
            {
                case 1: //Check Balance
                    System.out.println("\nAccount No -> " + a1.getAccountNo());
                    if(a1 instanceof Savings)
                    {
                    	Savings temp=(Savings)a1 ;
                    	System.out.println("\nBalance -> " + temp.getBalance());
                    }
                    else
                    {
                    	Current temp=(Current)a1;
                    	System.out.println("\nBalance -> " + temp.getBalance());
                    }
                    break;
                
                case 2: //Deposit
                    System.out.println("\nEnter the amount -> ");
                    am = input.nextDouble();
                    
                    try
                    {
                        a1.deposit(am);
                        System.out.println("\nDeposit Successful");
                        System.out.println("\nAccount Number -> " + a1.getAccountNo());
                        System.out.println("\nBalance -> " + a1.getBalance());
                    }
                    catch(MyException e)
                    {
                        System.out.println("\nDeposit Unsuccessful");
                    }
                    break;
                    
                case 3: //Withdraw
                    
                    try
                    {
                            System.out.println("\nEnter an amount -> ");
                            am = input.nextDouble();
                            a1.withdraw(am);
                            System.out.println("Withdraw Successful");
                            System.out.println("Account Number -> " + a1.getAccountNo());
                            System.out.println("Balance -> " + a1.getBalance());
                    }
                    catch(MyException err)
                    {
                        System.out.println("Withdraw Unsuccessful");
                    }
                    break;
                
                case 4:  //Exit
                    break;
                    
                default:
                    System.out.println("WRONG CHOICE!!!");
                    break;
            }
        }while(ch!=4);
    }
}