/*Author smith christian
  date 12.2.2016
  program Polynomial
*/
package linked_list;
import java.util.*;
public class Savings extends Account
{
    private double roi;
    
    int Acc_DateofCreation_year,Acc_DateofCreation_month,Acc_DateofCreation_day;
   
    GregorianCalendar Acc_LastUpdate=new GregorianCalendar(Acc_DateofCreation_year,Acc_DateofCreation_month,Acc_DateofCreation_day);

    
    public Savings(String no,double bal,String date,double rate)        //constructor
    {
        super(no,bal,date);
        
        Acc_DateofCreation_day=Integer.parseInt(Acc_DateofCreation.substring(0,2));
        Acc_DateofCreation_month=Integer.parseInt(Acc_DateofCreation.substring(3,5));
        Acc_DateofCreation_year=Integer.parseInt(Acc_DateofCreation.substring(6,10));        
        
        if(rate>0)
        {
            roi = rate;
        }
        else
        {
        	System.out.println("Wrong entry!!!");
        }
    }
    
    public boolean setRoi(float rate)
    {
        if(rate > 0)
        {
            roi = rate;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public double getRoi()        //to get rate of interest
    {
        return roi;
    }
    
    public void withdraw(double amt) throws MyException        //withdraw amount
    {
        try
        {
            if(amt <= 0)
                throw new MyException("Positive Amout Required!");
            else if(amt > balance)
                throw new MyException("Insufficient Balance!");
            else
            {
                balance -= amt;
            }
        }
        catch(MyException err)
        {
            throw err;
        }
    }
    
    public double getBalance()
    {
    	Calendar now = Calendar.getInstance();
        
        int Acc_DateofJoining_day,Acc_DateofJoining_month,Acc_DateofJoining_year;
    
        Acc_DateofJoining_day=Integer.parseInt(Acc_DateofCreation.substring(0,2));
        Acc_DateofJoining_month=Integer.parseInt(Acc_DateofCreation.substring(3,5));
        Acc_DateofJoining_year=Integer.parseInt(Acc_DateofCreation.substring(6,10));        
    	GregorianCalendar joining =new GregorianCalendar(Acc_DateofJoining_year,Acc_DateofJoining_month,Acc_DateofJoining_day);
    	if(Acc_LastUpdate.getTimeInMillis()==joining.getTimeInMillis())
    	{
	    	GregorianCalendar curr_date =new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)-1, now.get(Calendar.DATE));
	    	
	    	long days = (curr_date.getTimeInMillis()-joining.getTimeInMillis())/(1000*60*60*24);
                balance*=Math.pow((1+(roi/400)),days/90);
	    	Acc_LastUpdate.setTimeInMillis(curr_date.getTimeInMillis());
    	}       
    	return balance;
    }
}


