/* Author  : smith christian
   Date    : 13/3/2016
   Program : Program of Employee
*/


import java.util.Scanner;

class employee
{
    String nm,add,nu,dob,jod;
    double GS,NS;
    Scanner input = new Scanner(System.in);
    int sl,DA,HRA,TA,PT,TDS;
    public void details()
    {
        System.out.println("Enter the name of an employee");
        nm =input.nextLine();
        System.out.println("Enter the address of contact number");
        add=input.nextLine();
        do{
        System.out.println("add a contact number:");
        nu=input.nextLine();
         }while(nu.length()!=10);
        System.out.println("Enter the date of birth  (DD/MM/YYYY) ");
        dob=input.nextLine();
        System.out.println("Enter the joining date   (DD/MM/YYYY) ");
        jod=input.nextLine();
    }
    public void display()
    {
        System.out.println("Name of an employee"+nm);
        System.out.println("Address of contact number"+add);
        System.out.println("Contact number:"+nu);
        System.out.println("Date of birth: "+dob);
        System.out.println("Joining date"+jod);
    }
  
   
}
class permanent extends employee
{
    Scanner input = new Scanner(System.in);
    
    
    permanent(int da,int hra,int ta,int pt)
    {
        
        DA=da;
        HRA=hra;
        TA=ta;
        PT=pt;
        System.out.println("Enter da (0 to use default value)");
        da=input.nextInt();
        if(da!=0)
        DA=da;
        System.out.println("Enter HRA(0 to use default value)");
        hra=input.nextInt();
        if(hra!=0)
        HRA=hra;
        System.out.println("Enter TA(0 to use default value)");
        ta=input.nextInt();
        if(ta!=0)
        TA=ta;
        
       System.out.println("Enter PT(0 to use default value)");
       pt=input.nextInt();
       if(pt!=0)
        PT=pt;
       
       
    }
         void salary()
     { 
        System.out.println("Enter the salary from user");
        sl=input.nextInt();
       this.DA=(sl*this.DA)/100;
        this.HRA=(sl*this.HRA)/100;
         GS=DA+TA+HRA+sl;
        NS=GS-PT;
        System.out.println("Gross Salary is : "+GS);
        System.out.println("Net Salary is : "+NS);
    }
 
}
class adhoc extends employee
{ 
    
    double PT;
   adhoc(double  pt)
    {
        PT=pt;
        System.out.println("Enter the value of PT (0 to use default value)");
        pt=input.nextInt();
        if(pt!=0)
        {
            PT=pt;
        }
    }
    Scanner input = new Scanner(System.in);
       void salary(int s)
       {
        double tds=((s-PT)*10)/100;
        NS=s-PT-tds;
        System.out.println("Gross Salary is : "+s);
        System.out.println("Net Salary is : "+NS);
       }
}
public class Employee1
{
    
    public static void main(String[] args)
    { 
        
        Scanner input = new Scanner(System.in);
        int ch;
      
     
        System.out.println("1.Permanent Employees");
          System.out.println("2.Adhoc Employees");
           System.out.println("Enter your choice");
            ch=input.nextInt();
        
        switch(ch)
        {
            case 1:
                permanent P=new permanent(50,25,950,200);
                 P.details();
                  P.display();
                   P.salary();
                break;
            case 2:
                adhoc A = new adhoc(200);
                A.details();
                A.salary(1000);
                A.display();
                
  
                break;
            default:
                System.out.print("INVALID CHOICE");
                    
        }
    }
}