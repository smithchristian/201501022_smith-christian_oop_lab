/* Author  : smith christian
   Date    : 18/3/2016
   Program : Program of vehicle
*/

import java.util.Scanner;

class node
{
    String name,type,color;
    node next;
    node(String name,String color,String type)
    {
        this.name = name;
        this.color = color;
        this.type = type;
        next=null;
    }

    void display()
    {
        System.out.println("CUSTOMER NAME: "+name);
        System.out.println("Vehicle Colour: "+color);
        System.out.println("Vehicle Type: "+type);
    }
}

class list
{
    public node fstnode, lastnode;

	list() {
		fstnode = null;
		lastnode = null;
	}
    void insertnode(String name,String color,String type) {
		node n1 = new node(name,color,type);
		n1.next = null;
		if(fstnode == null) {
			fstnode = lastnode = n1;
			System.out.println("Linked list created successfully!");
		}
		else {
			lastnode.next = n1;
			lastnode = n1;
			System.out.println("Node inserted successfully!");
		}
	}
    void update() 
    {
		int count = 0, number, i;
		node n, n1;
		Scanner input = new Scanner(System.in);
		for(n = fstnode; n != null; n = n.next)
		count++;
		display();
		n = n1 = fstnode;
                String name,color,type;
		System.out.println(count+" nodes available here!");
		System.out.println("Enter the node number which you want to update:");
		number =input.nextInt();
                System.out.println("Enter your name: ");
                name=input.next();
                System.out.println("Enter the colour: ");
                color=input.next();
                System.out.println("Enter  the type of vehicle : ");
                type=input.next();
                node n2 = new node(name,color,type);
                if(number != 1) {
			if(number <= count) {
			for(i = 2; i <= number; i++)
			n = n.next;
			for(i = 2; i <= number-1; i++)
		        n1 = n1.next;
                        n1.next = n2;
                        n2.next = n.next;
                        n.next = null;
		        n = null;
			}
			else
			System.out.println("Wrong node number!\n");
		}
		else {
                    System.out.println("here");
                                            
			fstnode = n2;
                        n2.next = n.next;
                        n = null;
		}

		System.out.println("Node has been deleted successfully!\n");
	}
    	void delete() 
        {
		int count = 0, number, i;
		node n, n1;
		Scanner input = new Scanner(System.in);
		
		for(n = fstnode; n != null; n = n.next)
			count++;
		display();
		n = n1 = fstnode;
		System.out.println(count+" nodes available here!");
		System.out.println("Enter the node number which you want to delete:");
		number = Integer.parseInt(input.nextLine());
		if(number != 1) {
		if(number <= count) {
                for(i = 2; i <= number; i++)
         	n = n.next;
			for(i = 2; i <= number-1; i++)
			n1 = n1.next;
			n.next = n.next;
			n.next = null;
			n = null;
			}
			else
				System.out.println("Invalid node number!\n");
		}
		else {
			fstnode = n.next;
			n = null;
		}

		System.out.println("Node has been deleted successfully!\n");
    }   

    void display() 
    {
		node n = fstnode;
		System.out.println("List of node:");
		while(n != null) {
			n.display();
			n = n.next;
		}
	}
}

class engine
{
    String en_type;
    Scanner in = new Scanner(System.in);


    public String gettype()
    {
                System.out.print("\nEnter engine type: ");
        en_type = in.next();
        return en_type;
    }
}
class vehicle
{
    engine e = new engine();
    
    public String en_type()
    {
        return e.gettype();
    }
}
class TwoWheeler extends vehicle
{

    public String gettwovehtype() 
    {
      String en_ty;   
      en_ty = en_type();
      return en_ty;
    }
}
abstract class FourWheeler extends vehicle
{
     public abstract String get_veh_type();
}
class PrivateCar extends FourWheeler
{
   
    public String get_veh_type() {
     String a="Private vehicle";
        return a;
    }
    
}
class CommercialCar extends FourWheeler
{
        public String get_veh_type() {
     String a="Commercial vehicle";
        return a;
    }
}
class MyVehicle
{
    public static void main(String[] args)
    {
        int choice,ch,c;
        String name,color;
        Scanner sc = new Scanner(System.in);
        list ll = new list();

        do
        {
            System.out.println("1 : Create");
            System.out.println("2 : Update");
            System.out.println("3 : Delete");
            System.out.println("4 : Display");
            System.out.println("0 : Exit");
            System.out.println("\nEnter your Choice: ");
            choice = sc.nextInt();
            switch(choice)
            {
                case 1:     //Create
                    System.out.println("Enter your name:  ");
                    name = sc.next();
                    System.out.println("Enter the color of vehicle:  ");
                    color = sc.next();
                    if(choice == 1)
                    {
                        System.out.println("Vehicle Type");
                        System.out.println("1 : Two Wheeler");
                        System.out.println("2 : Four Wheeler");
                        System.out.println("\nEnter vehicle type: ");
                        ch = sc.nextInt();
                        if(ch == 1)
                        {
                          TwoWheeler v1 = new TwoWheeler(); 
                          ll.insertnode(name,color,v1.gettwovehtype());
                        }
                        else if(ch == 2)
                        {
                            System.out.println("Four Wheeler Type");
                            System.out.println("1 : Private Car");
                            System.out.println("2 : Commercial Car");
                            System.out.println("\nEnter four wheeler type: ");
                            c = sc.nextInt();
                            if(c == 1)
                            {
                                FourWheeler v1 = new PrivateCar();
                                ll.insertnode(name,color,v1.get_veh_type());
                            }
                            else
                            {
                                FourWheeler v1 = new CommercialCar();
                                ll.insertnode(name,color,v1.get_veh_type());
                            }
                        }
                    }
                    ll.display();
                    break;
                case 2:  //update   
                    ll.update();
                    ll.display();
                    break;
                case 3:    //delete 
                    ll.delete();
                    ll.display();
                    break;
                case 4:     //display
                    ll.display();
                case 0:     
                    break;
                default:
                    System.out.println("INVALID CHOICE!!!");
                   break;
            }
        }while(choice != 0);
    }
}
